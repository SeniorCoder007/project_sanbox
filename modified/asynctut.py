import requests
import asyncio




async def get_resp(wait,url,say):
	await asyncio.sleep(wait)
	print(say)
	resp = requests.get(url)
	return resp


async def main():
	loop = asyncio.get_event_loop()
	resp1 = loop.create_task(get_resp(5,'http://www.google.com','first'))
	resp2 = loop.create_task(get_resp(2,'http://www.google.com','second'))
	await resp1
	await resp2
	print(resp1.result())
	print(resp2.result)

	


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()