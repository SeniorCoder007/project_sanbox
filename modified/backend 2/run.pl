#!/usr/bin/perl
use strict;
use warnings;

my $windows = ($^O =~ /Win/) ? 1 : 0;
my $python = 'python3';
my $action = 'run';
if(scalar(@ARGV) > 0) {
	$action = $ARGV[0];
}
if(($action eq 'run') || ($action eq 'start')) {
	&doStart;
}
elsif(($action eq 'quit') || ($action eq 'stop')) {
	&doStop;
}
elsif(($action eq 'restart')) {
	&doStop;
	&doStart;
}
else {
	print "Unknow action $action.\n";
}

sub doStart
{
	if($windows) {
		system("$python main.py --action=service");
	}
	else {
		system("nohup $python main.py --action=service >./data/temp/smsinside.out 2>&1 </dev/null &");
	}
}

sub doStop
{
	system("$python main.py --action=quit");
}
