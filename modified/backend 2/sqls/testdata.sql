START TRANSACTION;

INSERT IGNORE INTO inmate
(id, email, inmate_number, phone_number,
	credit, create_time, trial_expiry, billing_day, service_expiry,
	corrlinks_password, corrlinks_first_name, corrlinks_last_name, corrlinks_session)
VALUES
(1, 'tonysoprano6959@gmail.com', '04068095', '+12029085500',
	0, UNIX_TIMESTAMP(), UNIX_TIMESTAMP(ADDDATE(NOW(), INTERVAL 30 DAY)), 1, UNIX_TIMESTAMP(ADDDATE(NOW(), INTERVAL 30 DAY)),
	'21Corrlinks$', 'First', 'Last', ''),
(2, 'tonysoprano6959+a@gmail.com', '94068095', '+12029085501',
	0, UNIX_TIMESTAMP(), 0, 1, 0,
	'smCgxWoaqv16', 'aaa', 'bbb', '');

INSERT IGNORE INTO phone_number
(id, inmate_number, phone_number)
VALUES
(1, '04068095', '+12029085500');

COMMIT;
