import common.util as util

import datetime

def test_splitEmailAddress() :
	assert util.splitEmailAddress('abc@defg.com') == { 'name' : 'abc', 'domain' : 'defg.com' }
	assert util.splitEmailAddress('abc@defg.com.cn') == { 'name' : 'abc', 'domain' : 'defg.com.cn' }
	assert util.splitEmailAddress('abc.defg.com') == { 'name' : 'abc.defg.com', 'domain' : '' }
	assert util.splitEmailAddress('abc@defg@aaa.com') == { 'name' : '', 'domain' : '' }

def test_emailAddressHasDomain() :
	assert util.emailAddressHasDomain('abc@defg.com', 'defg.com')
	assert util.emailAddressHasDomain('abc@defg.com.ab.cd', 'defg.com.ab.cd')
	assert not util.emailAddressHasDomain('abc.defg.com', 'defg.com')

def test_roundDownTimestampToMidnight() :
	assert util.roundDownTimestampToMidnight(datetime.datetime(2019, 5, 3, 8, 5, 38).timestamp()) == datetime.datetime(2019, 5, 3, 0, 0, 0).timestamp()
