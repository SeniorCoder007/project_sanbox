import workers.baseworker as baseworker
import models.inmatemodel as inmatemodel
import models.taskmodel as taskmodel
import models.corrlinksmessagemodel as corrlinksmessagemodel
import app.constants as constants
import app.corrlinks as corrlinks
import app.apputil as apputil
import common.util as util
import common.logger as logger

class CorrLinksMonitor(baseworker.BaseWorker) :
	def __init__(self, monitorCount, monitorIndex) :
		baseworker.BaseWorker.__init__(self)
		self._monitorCount = monitorCount
		self._monitorIndex = monitorIndex
		self._inmateModel = inmatemodel.InmateModel()
		self._pageIndex = 0
		self._pageSize = 10
		self._inmateIndex = 0
		self._inmateList = []
		self._doLoadInmates()

	def _doThreadRun(self) :
		while not self.shouldStop() :
			if self._inmateIndex < len(self._inmateList) :
				self._doProcessInmate(self._inmateList[self._inmateIndex])
				self._inmateIndex += 1
				util.sleepForMilliseconds(100)
			else :
				self._pageIndex += 1
				self._doLoadInmates()
				if len(self._inmateList) == 0 :
					self._pageIndex = 0
					self._doLoadInmates()
					self.sleepForSeconds(60 * 6)

	def _doProcessInmate(self, inmate) :
		if not apputil.canCheckCorrLinksMessages(inmate) :
			return
		corr = corrlinks.CorrLinks(inmate[inmatemodel.InmateModel.column_email], inmate[inmatemodel.InmateModel.column_corrlinks_password])
		messageList = corr.getInboxItemList(self.getConfig().debugOnlyFetchUnreadCorrLinksMessages())
		if util.isEmptyList(messageList) :
			return
		corrLinksMessageModel = corrlinksmessagemodel.CorrLinksMessageModel()
		for message in messageList :
			if message == None :
				continue
			content = util.getDictValue(message, 'details')
			if content == None :
				continue
			hash = apputil.hashCorrLinksMessage(
				inmate[inmatemodel.InmateModel.column_inmate_number],
				content['subject'],
				content['message']
			)
			if corrLinksMessageModel.getMessageByHash(hash) != None :
				continue;
			taskmodel.TaskModel().addTask({
				'type' : constants.TaskType.receivedMessageFromCorrLinks,
				'content' : content
			})
			corrLinksMessageModel.addMessage(
				inmate[inmatemodel.InmateModel.column_inmate_number],
				content['subject'],
				content['message'],
				content['date'],
				hash
			)

	def _doLoadInmates(self) :
		self._inmateIndex = 0
		self._inmateList = self._inmateModel.getInmatesByShard(
			self._pageSize,
			self._pageSize * self._pageIndex,
			self._monitorCount,
			self._monitorIndex
		)
		if self._inmateList == None :
			self._inmateList = []
