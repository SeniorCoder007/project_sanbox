import workers.taskhandlers.taskhandler as taskhandler
import models.inmatemodel as inmatemodel
import models.taskmodel as taskmodel
import models.phonenumbermodel as phonenumbermodel
import app.constants as constants
import app.applang as applang
import app.apputil as apputil
import common.util as util
import common.logger as logger

class RefillFund(taskhandler.TaskHandler) :
	def __init__(self) :
		taskhandler.TaskHandler.__init__(self)

	def _doProcess(self) :
		logger.debug("Task RefillFund")
		task = self.getTaskContent()
		inmateNumber = task['inmateNumber']
		inmateNumber = apputil.normalizeInmateNumber(inmateNumber)
		if not apputil.isValidInmateNumber(inmateNumber) :
			logger.error('RefillFund._doProcess: wrong inmate number %s' % (inmateNumber))
			return taskhandler.HandlerResult.failed

		amount = apputil.dollarToCredit(task['amount'])
		email = util.trim(task['email'])
		inmateModel = inmatemodel.InmateModel()
		inmate = inmateModel.getInmateByInmateNumber(inmateNumber)
		if inmate == None :
			inmate = apputil.createInmateAccount(inmateNumber, False)
		inmateModel.addFund(inmateNumber, amount)

		# reload inmate since the credit was changed
		inmate = inmateModel.getInmateByInmateNumber(inmateNumber)
		apputil.checkBilling(inmate)
		apputil.checkAddInmatePhoneNumber(inmate)

		if not util.isEmptyText(email) :
			params = {
				'inmateNumber' : inmate[inmateModel.column_inmate_number],
				'email' : inmate[inmateModel.column_email],
				'phoneNumber' : inmate[inmateModel.column_phone_number],
				'amount' : task['amount']
			}
			taskmodel.TaskModel().addTask({
				'type' : constants.TaskType.sendMail,
				'content' : {
					'to' : email,
					'subject' : applang.AppLang.getLang().getText('message.depositeMailSubject', params),
					'message' : applang.AppLang.getLang().getText('message.depositeMailBody', params),
				}
			})
		return taskhandler.HandlerResult.success
