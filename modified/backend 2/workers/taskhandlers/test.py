import workers.taskhandlers.taskhandler as taskhandler
import common.util as util
import common.logger as logger

class Test(taskhandler.TaskHandler) :
	def __init__(self) :
		taskhandler.TaskHandler.__init__(self)

	def _doProcess(self) :
		task = self.getTaskContent()
		logger.debug("Task Test: content %s" % (util.encodeJson(task)))
		return taskhandler.HandlerResult.success
