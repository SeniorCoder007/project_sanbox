import workers.taskhandlers.taskhandler as taskhandler
import app.constants as constants
import app.corrlinks as corrlinks
import app.apputil as apputil
import app.appconfig as appconfig
import app.applang as applang
import models.inmatemodel as inmatemodel
import app.signalwire as signalwire
import common.util as util
import common.logger as logger

class ReceivedMessageFromSms(taskhandler.TaskHandler) :
	def __init__(self) :
		taskhandler.TaskHandler.__init__(self)

	def _doProcess(self) :
		logger.debug("Task ReceivedMessageFromSms")
		task = self.getTaskContent()
		if constants.MessageDirection(task['direction']) != constants.MessageDirection.inbound :
			return taskhandler.HandlerResult.success
		fromPhoneNumber = task['fromPhoneNumber']
		if fromPhoneNumber == None :
			return taskhandler.HandlerResult.success
		toPhoneNumber = task['toPhoneNumber']
		message = task['message']

		inmateModel = inmatemodel.InmateModel()
		inmate = inmateModel.getInmateByPhoneNumber(toPhoneNumber)
		if inmate == None :
			logger.error("ReceivedMessageFromSms: unregistered phone number %s" % (toPhoneNumber))
			return taskhandler.HandlerResult.failed

		if apputil.isServiceDisabled(inmate) :
			myPhoneNumber = appconfig.AppConfig.getConfig().getCompanySmsPhoneNumber()
			signalWire = signalwire.SignalWire.getSignalWire()
			message = applang.AppLang.getLang().getText("message.serviceDisabledBody", {
				'message' : message,
				'url' : self.getConfig().getUrlWebsite(),
			})
			signalWire.sendMessage(myPhoneNumber, fromPhoneNumber, message)
			return taskhandler.HandlerResult.success

		subject = "From #%s -- to reply do not change the number" % (fromPhoneNumber)

		corr = corrlinks.CorrLinks(inmate[inmateModel.column_email], inmate[inmateModel.column_corrlinks_password])

		if self.getConfig().debugAllowSendMessage(subject + message) :
			logger.debug('ReceivedMessageFromSms: send message to CorrLinks')
			message = apputil.formatCorrLinksMessage(inmate, message)
			corr.sendNewMessage(inmate[inmateModel.column_inmate_number], subject, message)
		else :
			inmate[inmateModel.column_corrlinks_session] = ''
			debugMessage = "\n\nsubject:%s\nmessage:%s\ninmate:%s\nfrom:%s\n\n" % (subject, message, util.encodeJson(inmate), fromPhoneNumber)
			logger.debug('ReceivedMessageFromSms: write message to debug file.')
			util.appendFile('../web/public/debug.txt', debugMessage)

		return taskhandler.HandlerResult.success
