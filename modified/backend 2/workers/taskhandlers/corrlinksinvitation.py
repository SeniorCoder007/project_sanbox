import workers.taskhandlers.taskhandler as taskhandler
import models.inmatemodel as inmatemodel
import models.phonenumbermodel as phonenumbermodel
import app.corrlinks as corrlinks
import app.appconfig as appconfig
import app.apputil as apputil
import common.util as util
import common.logger as logger

class CorrLinksInvitation(taskhandler.TaskHandler) :
	def __init__(self) :
		taskhandler.TaskHandler.__init__(self)

	def _doProcess(self) :
		logger.debug("Task RefillFund")
		task = self.getTaskContent()
		email = task['email']
		code = task['code']
		inmateModel = inmatemodel.InmateModel()
		inmate = inmateModel.getInmateByEmail(email)
		if inmate == None :
			inmate = apputil.createInmateAccount('', True, email)
		if inmate == None :
			logger.error('Unfound inmate invitation of email ' + email)
			return taskhandler.HandlerResult.success

		if util.isEmptyText(inmate[inmateModel.column_corrlinks_password]) :
			inmate[inmateModel.column_corrlinks_password] = util.getRandomMixedText(upperCount = 2, lowerCount = 8, digitCount = 2)
		if util.isEmptyText(inmate[inmateModel.column_corrlinks_first_name]) :
			inmate[inmateModel.column_corrlinks_first_name] = appconfig.AppConfig.getConfig().getFirstNameDict().getRandomLine()
		if util.isEmptyText(inmate[inmateModel.column_corrlinks_last_name]) :
			inmate[inmateModel.column_corrlinks_last_name] = appconfig.AppConfig.getConfig().getLastNameDict().getRandomLine()

		where = {}
		where[inmateModel.column_id] = inmate[inmateModel.column_id]
		inmateModel.update(inmateModel.table, inmate, where, [
			inmateModel.column_corrlinks_password,
			inmateModel.column_corrlinks_first_name,
			inmateModel.column_corrlinks_last_name
		])

		corr = corrlinks.CorrLinks(inmate[inmateModel.column_email], inmate[inmateModel.column_corrlinks_password])
		corr.register(
			email = inmate[inmateModel.column_email],
			password = inmate[inmateModel.column_corrlinks_password],
			firstName = inmate[inmateModel.column_corrlinks_first_name],
			lastName = inmate[inmateModel.column_corrlinks_last_name],
			code = code
		)

		logined = False
		for i in range(3) :
			if corr.login() :
				logined = True
				break

		if not logined :
			return taskhandler.HandlerResult.postpone

		inmateNumber = corr.findInmateNumber()
		if inmateNumber == None :
			return taskhandler.HandlerResult.postpone

		existingInmate = inmateModel.getInmateByInmateNumber(inmateNumber)
		if existingInmate == None :
			phoneNumber = phonenumbermodel.PhoneNumberModel().usePhoneNumber(inmateNumber)
			inmateModel.setInmateNumberAndPhoneNumber(inmate[inmateModel.column_email], inmateNumber, phoneNumber)
		else :
			if inmate[inmateModel.column_id] != existingInmate[inmateModel.column_id] :
				inmateModel.deleteByEmail(inmate[inmateModel.column_email])
			inmateModel.setEmailAndPassword(inmateNumber, inmate[inmateModel.column_email], inmate[inmateModel.column_corrlinks_password])

		apputil.sendInmateWelcomeEmail(inmateNumber)

		return taskhandler.HandlerResult.success

