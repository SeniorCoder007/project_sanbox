import app.application as application
import app.appconfig as appconfig
import app.appinit as appinit

class Service :
	def run(self) :
		config = appconfig.AppConfig()
		app = application.Application(config)
		appinit.initApplication(app)
		app.run()

