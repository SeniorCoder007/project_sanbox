import common.util as util
import common.logger as logger
import common.textlines as textlines
import app.applang as applang

from datetime import datetime

class AppConfig :
	_instance = None
	
	@staticmethod
	def getConfig() :
		if AppConfig._instance == None :
			AppConfig._instance = AppConfig()
		return AppConfig._instance

	def __init__(self) :
		AppConfig._instance = self

		self._configFileName = 'data/config.json'
		self._config = None
		self._doLoadConfig()
		applang.AppLang.getLang()
		self._firstNameDict = textlines.TextLines('data/firstname.txt')
		self._lastNameDict = textlines.TextLines('data/lastname.txt')

	def _doLoadConfig(self) :
		self._config = util.decodeJson(util.readFileContent(self._configFileName))
		if self._config == None :
			logger.error("Can't load config!")
		logger.setLogLevel(self.getLogLevel())
		logger.setLogFile(self.getLogFile())

	def getFirstNameDict(self) :
		return self._firstNameDict

	def getLastNameDict(self) :
		return self._lastNameDict
		
	def _doGetValue(self, name, default = None) :
		return util.getDictValue(self._config, name, default)
		
	def _doGetNestedValue(self, firstLevelName, name, default = None) :
		return util.getDictValue(util.getDictValue(self._config, firstLevelName), name, default)
		
	def getLogLevel(self) :
		return self._doGetNestedValue('log', 'level', 'info')

	def getLogFile(self) :
		return self._doGetNestedValue('log', 'file')

	def getDatabaseConfig(self) :
		return self._doGetValue('database')

	def getTaskProcessorCount(self) :
		return self._doGetNestedValue('task', 'processorCount', 8)
		
	def getCorrLinksMonitorCount(self) :
		return self._doGetNestedValue('corrLinks', 'monitorCount', 8)
		
	def getSignalWireProjectKey(self) :
		return self._doGetNestedValue('signalWire', 'projectKey')
		
	def getSignalWireToken(self) :
		return self._doGetNestedValue('signalWire', 'token')
		
	def getSignalWireSpaceUrl(self) :
		return self._doGetNestedValue('signalWire', 'spaceUrl')
		
	def getSignalWireTimeAfter(self) :
		timeAfter = self._doGetNestedValue('signalWire', 'timeAfter')
		if timeAfter == None :
			return 0
		dt = datetime.strptime(timeAfter,'%Y-%m-%dT%H:%M:%SZ')
		return util.dateTimeToTimestamp(dt)
		
	def isDebugEnabled(self) :
		return self._doGetNestedValue('debug', 'enabled', False)
		
	def debugSkipCorrLinksEmailAddressCheck(self) :
		return self.isDebugEnabled() and self._doGetNestedValue('debug', 'skipCorrLinksEmailAddressCheck', False)

	def debugOnlyFetchUnreadCorrLinksMessages(self) :
		if self.isDebugEnabled() :
			return self._doGetNestedValue('debug', 'onlyFetchUnreadCorrLinksMessages', True)
		else :
			return True

	def debugAllowSendMessage(self, message) :
		if self.isDebugEnabled() :
			text = self._doGetNestedValue('debug', 'onlySendMessageIncludeText', '')
			if not util.isEmptyText(text) :
				if util.containsText(message, text) :
					return True
				else :
					return False
			return True
		else :
			return True

	def debugUseProxy(self) :
		if self.isDebugEnabled() :
			return self._doGetNestedValue('debug', 'useProxy', True)
		else :
			return True

	def isTargetPhoneNumberAllowed(self, phoneNumber) :
		if self.isDebugEnabled() :
			return not (phoneNumber in self._doGetNestedValue('debug', 'ignoredTargetPhoneNumbers', []))
		else :
			return True
	
	def getInmateTrialDays(self) :
		return self._doGetNestedValue('inmate', 'trialDays', 30)

	def getInmatePhoneRecycleDays(self) :
		return self._doGetNestedValue('inmate', 'phoneRecycleDays', 30)

	def getInmateMonthlyFee(self) :
		return self._doGetNestedValue('inmate', 'monthlyFee')

	def getUrlWebsite(self) :
		return self._doGetNestedValue('url', 'website', '')

	def getUrlRefill(self) :
		url = self._doGetNestedValue('url', 'refill')
		if not url :
			url = self.getUrlWebsite()
		return url

	def getSmtp(self) :
		return self._doGetValue('smtp')

	def getCompanySmsPhoneNumber(self) :
		return self._doGetNestedValue('company', 'smsPhoneNumber')
