import common.database as database
import common.util as util
import common.logger as logger
import app.signalwire as signalwire
import app.appsession as appsession

import json
import time
import signal
import sys
import os

class Application :
	_instance = None
	
	@staticmethod
	def getApplication() :
		return Application._instance
		
	def __init__(self, config) :
		Application._instance = self
		self._config = config
		self._session = appsession.AppSession()
		self._shouldStop = False
		self._database = None
		self._workerList = []
		self._database = database.Database(self._config.getDatabaseConfig())
		self._signalWire = signalwire.SignalWire(
			self._config.getSignalWireProjectKey(),
			self._config.getSignalWireToken(),
			self._config.getSignalWireSpaceUrl()
		)
		
	def getConfig(self) :
		return self._config

	def run(self) :
		self._session.load()
		if self._session.isRunning() :
			print("Warnning: application has been running. Quit.")
			return

		signal.signal(signal.SIGINT, lambda sig, frame : self.stop())

		self._session.setRunning(True)
		self._session.setQuit(False)
		self._session.save()

		logger.info("===========================")
		logger.info("Application started")

		try :
			for worker in self._workerList :
				worker.run()

			self._doMainLoop()
		finally :
			self._database.close()
			self._session.setRunning(False)
			self._session.save()
			logger.info("Application stopped")
	
	def stop(self) :
		if self._shouldStop :
			sys.exit(0)
		else :
			self._shouldStop = True

	def appendWorker(self, worker) :
		self._workerList.append(worker)
		return worker

	def _doMainLoop(self) :
		while not self._shouldStop :
			if self._areAllThreadsDone() :
				self._shouldStop = True
				break

			self._session.checkReload()
			if self._session.shouldQuit() :
				self._shouldStop = True
				break

			util.sleepForSeconds(3)
			
		for thread in self._workerList :
			thread.stop()
		
	def _areAllThreadsDone(self) :
		for thread in self._workerList :
			if thread.isRunning() :
				return False
		return True
