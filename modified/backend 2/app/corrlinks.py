import common.util as util
import common.logger as logger
import common.htmlparser as htmlparser
import app.application as application
import app.corrlinksparser as corrlinksparser
import app.apputil as apputil
import app.appconfig as appconfig
import app.proxy as proxy
from app.proxy_handler import Proxy_handler
import models.inmatemodel as inmatemodel

import requests
import pickle
import socks

requests.adapters.DEFAULT_RETRIES = 10

userAgentList = [
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/17.17134',
	'Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
	'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0',
	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
	'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36',
]

class CorrLinks :
	def __init__(self, email, password) :
		self._email = email
		self._password = password
		self._inmateNumberChecked = False
		self._session = None
		userAgent = userAgentList[util.getRandomInt(0, len(userAgentList))]
		self._headers = {
			'User-Agent' : userAgent,
			'Connection' : 'close',
		}
		self._proxy = None
	
	def _getSession(self, new = False) :
		if new :
			self._session = requests.session()
			self._session.keep_alive = False
		else :
			if self._session == None :
				self._session = requests.session()
				self._session.keep_alive = False
				sessionText = inmatemodel.InmateModel().getCorrLinksSession(self._email)
				if not util.isEmptyText(sessionText) :
					self._session.cookies.update(pickle.loads(util.hexToBin(sessionText)))
		return self._session
		
	def _saveSession(self) :
		sessionText = util.binToHex(pickle.dumps(self._session.cookies))
		inmatemodel.InmateModel().setCorrLinksSession(self._email, sessionText)
		
	def _makeUrl(self, url) :
		url = 'https://www.corrlinks.com' + url
		return url

	def _shouldUseProxy(self, url) :
		return url.find('corrlinks.com') >= 0;

	def _isLoginPage(self, content) :
		return (util.containsText(content, 'ctl00$mainContentPlaceHolder$loginUserNameTextBox')
			and util.containsText(content, 'ctl00$mainContentPlaceHolder$loginPasswordTextBox')
			and util.containsText(content, 'ctl00$mainContentPlaceHolder$loginButton')
		)
			
	def _doRequestWithoutLogin(self, url, method, data = None, headers = None) :
		try :
			session = self._getSession()
			
			h = self._headers
			if headers != None :
				h = util.mergeDicts(h, headers)
			session.headers.update(h)

			proxyToUse = None

						#if self._shouldUseProxy(url) :
			#	if self._proxy == None :
			#		if appconfig.AppConfig.getConfig().debugUseProxy():
			#			self._proxy = proxy.getRandomProxy()
			#	if self._proxy == None :
			#		logger.warning('Proxy is not enabled')
			#	proxyToUse = self._proxy

			#proxyToUse = None # disable proxy temporarily

			while True:

				if appconfig.AppConfig.getConfig().debugUseProxy():
					self._proxy = self.pr_handler.set_new_proxy()
				if self._proxy == None :
					logger.warning('Proxy is not enabled')
				proxyToUse = self._proxy
				self._proxy = None # always use new proxy
				response = None
				try:
					if method == 'get' :
						response = session.get(url, data = data, verify = True, proxies = proxyToUse)
					else :
						response = session.post(url, data = data, verify = True, proxies = proxyToUse)

					self.pr_handler.reset_counter()
					break
				except:
					self.pr_handler.increment_counter()
					continue


			#print(response.status_code)
			return response
		except Exception as e:
			logger.error('CorrLinks._doRequestWithoutLogin: request error. ' + str(getattr(e, 'message', repr(e))))
			self._proxy = None
			return None
		except :
			logger.error('CorrLinks._doRequestWithoutLogin: request error. Not normal Exception.')
			self._proxy = None
			return None
	
	def _doRequest(self, url, method, data = None, headers = None) :
		response = self._doRequestWithoutLogin(url, method, data, headers)
		if response == None :
			logger.error('CorrLinks._doRequest: response is None when loading ' + url)
			return None
		if self._isLoginPage(response.text) :
			if self.login() :
				response = self._doRequestWithoutLogin(url, method, data, headers)
			else :
				response = None

		return response
	
	def register(self, email, password, firstName, lastName, code) :
		self._inmateNumberChecked = False
		self._email = email
		self._password = password
		url = self._makeUrl('/SignUp.aspx')

		logger.debug("CorrLinks.register: start register email=%s" % (self._email))

		response = self._doRequestWithoutLogin(url, 'get')
		if response == None :
			logger.debug("CorrLinks.register: failed register email=%s, can't get first page" % (self._email))
			return None

		document = htmlparser.HtmlDocument(response.text)
		form = corrlinksparser.getRegisterForm(document)
		if form == None :
			logger.debug("CorrLinks.register: failed register email=%s, can't find register form" % (self._email))
			return None
		
		solvedToken = self.solveRecaptcha(document, url, False)
		if solvedToken == None :
			return None

		postData = {
			'g-recaptcha-response' : solvedToken,
			'ctl00$mainContentPlaceHolder$firstNameTextBox1' : firstName,
			'ctl00$mainContentPlaceHolder$lastNameTextBox1' : lastName,
			'ctl00$mainContentPlaceHolder$emailAddressTextBox' : email,
			'ctl00$mainContentPlaceHolder$emailAddressTextBox2' : email,
			'ctl00$mainContentPlaceHolder$passwordTextBox' : password,
			'ctl00$mainContentPlaceHolder$confirmPasswordTextBox' : password,
			'ctl00$mainContentPlaceHolder$requestCodeTextBox' : code,
			'ctl00$mainContentPlaceHolder$tocCheckBox' : 'on',
			'ctl00$mainContentPlaceHolder$nextButton' : form.getInputValue('ctl00$mainContentPlaceHolder$nextButton'),
			'__COMPRESSEDVIEWSTATE' : form.getInputValue('__COMPRESSEDVIEWSTATE'),
			'__VIEWSTATE' : form.getInputValue('__VIEWSTATE'),
			'__EVENTVALIDATION' : form.getInputValue('__EVENTVALIDATION'),
		}
		response = self._doRequestWithoutLogin(url, 'post', postData)
		if response == None :
			logger.debug("CorrLinks.register: failed register email=%s, submission page failed" % (self._email))
			return None

		self._saveSession()

		util.writeDebugFile('register.html', response.text)
		
		logger.debug("CorrLinks.register: succeeded register email=%s" % (self._email))

		return response.text

	def getInboxItemList(self, unreadOnly = True, includeMessage = True) :
		url = self._makeUrl('/Inbox.aspx')

		response = self._doRequest(url, 'get')
		if response == None :
			logger.error("CorrLinks.getInboxItemList: failed get all message items")
			return None

		document = htmlparser.HtmlDocument(response.text)
		form = corrlinksparser.getInboxForm(document)
		if form == None :
			logger.error("CorrLinks.getInboxItemList: failed to find the form")
			return None

		postData = form.inputsToParams()

		if unreadOnly :
			postData['__EVENTTARGET'] = 'ctl00$mainContentPlaceHolder$UnreadMessages'
			postData['__EVENTARGUMENT'] = ''
			postData['ctl00$mainContentPlaceHolder$UnreadMessages'] = 'on'
			response = self._doRequest(url, 'post', postData)
			if response == None :
				logger.error("CorrLinks.getInboxItemList: failed get unread message items")
				return None

		#util.writeDebugFile('inbox.html', response.text)
		document = htmlparser.HtmlDocument(response.text)
		itemList = corrlinksparser.getInboxItems(document)
		
		if includeMessage and itemList != None :
			for i, item in enumerate(itemList):
				postData['__EVENTTARGET'] = item['action']['target']
				postData['__EVENTARGUMENT'] = item['action']['argument']
				postData['ctl00$mainContentPlaceHolder$UnreadMessages'] = None
				postData['ctl00$mainContentPlaceHolder$startDateTextBox_PU_PN_MYP_PN_OK'] = None
				postData['ctl00$mainContentPlaceHolder$startDateTextBox_PU_PN_MYP_PN_Cancel'] = None
				postData['ctl00$mainContentPlaceHolder$updateButton'] = None
				postData['ctl00$mainContentPlaceHolder$UnreadMessages'] = None
				postData['ctl00$mainContentPlaceHolder$inboxGridView$ctl01$chkBxHeader'] = None
				postData['ctl00$mainContentPlaceHolder$inboxGridView$ctl02$chkBxSelect'] = None
				postData['ctl00$mainContentPlaceHolder$inboxGridView$ctl03$chkBxSelect'] = None

				#util.writeDebugFile('postdata.txt', str(postData))
				response = self._doRequest(url, 'post', postData)
				if response == None :
					logger.error("CorrLinks.getInboxItemList: failed get message")
					return None
				#util.writeDebugFile(item['action']['argument'] + '_msg.html', response.text)
				document = htmlparser.HtmlDocument(response.text)
				itemList[i]['details'] = corrlinksparser.getInboxMessage(document)
		
		return itemList
		
	def _doGetReceiverAddress(self, url, inmateNumber, document, form) :
		fieldName = corrlinksparser.getSendMessageAddressBoxFieldName(document, inmateNumber)
		if util.isEmptyText(fieldName) :
			logger.error("CorrLinks._doGetReceiverAddress: can't find field name")
			return None, None

		postData = form.inputsToParams()
		postData['ctl00$mainContentPlaceHolder$addressBox$clearButton'] = ''
		postData['ctl00$mainContentPlaceHolder$addressBox$okButton'] = 'OK'
		postData['ctl00$mainContentPlaceHolder$sendMessageButton'] = None
		postData['ctl00$mainContentPlaceHolder$saveButton'] = None
		postData['ctl00$mainContentPlaceHolder$cmdCancel'] = None
		
		postData[fieldName] = 'on'
		postData['__EVENTTARGET'] = 'ctl00$mainContentPlaceHolder$addressBox$okButton'
		postData['ctl00$mainContentPlaceHolder$addressBox$addressStringTextBox'] = 0
		postData['ctl00$mainContentPlaceHolder$addressBox$addressTextBox'] = ''
		postData['ctl00$mainContentPlaceHolder$subjectTextBox'] = ''
		postData['ctl00$mainContentPlaceHolder$messageTextBox'] = ''
		#postData['ctl00$topScriptManager'] = 'ctl00$topUpdatePanel|ctl00$mainContentPlaceHolder$addressBox$okButton'
		postData['ctl00$mainContentPlaceHolder$countDown'] = 13000
		postData['ctl00$mainContentPlaceHolder$lineCountDown'] = 100
		# Can't use Python True, it will crash the server
		postData['__ASYNCPOST'] = 'true'

		response = self._doRequest(url, 'post', data = postData)
		if response == None :
			logger.error("CorrLinks._doGetReceiverAddress: failed to get address")
			return None, None
			
		html = corrlinksparser.makeHtmlFromPiece(response.text)
		#util.writeDebugFile('address.html', html)
		address = corrlinksparser.getSendMessageReceiverAddress(htmlparser.HtmlDocument(html))
		return address, fieldName
		
	def sendNewMessage(self, inmateNumber, subject, message) :
		url = self._makeUrl('/NewMessage.aspx')

		response = self._doRequest(url, 'get')
		if response == None :
			logger.error("CorrLinks.sendNewMessage: failed get all message items")
			return False

		document = htmlparser.HtmlDocument(response.text)
		form = corrlinksparser.getSendMessageForm(document)
		if form == None :
			logger.error("CorrLinks.sendNewMessage: failed to find the form")
			return False
			
		address, fieldName = self._doGetReceiverAddress(url, inmateNumber, document, form)
		if address == None or fieldName == None :
			logger.error("CorrLinks.sendNewMessage: failed to find the address")
			return False

		postData = form.inputsToParams()
		#postData['ctl00$mainContentPlaceHolder$addressBox$addressTextBox'] = receiver + ';'
		postData['ctl00$mainContentPlaceHolder$subjectTextBox'] = subject
		postData['ctl00$mainContentPlaceHolder$messageTextBox'] = message
		postData['ctl00$mainContentPlaceHolder$sendMessageButton'] = 'Send'
		#postData['__EVENTTARGET'] = 'ctl00$mainContentPlaceHolder$sendMessageButton'
		postData['ctl00$mainContentPlaceHolder$countDown'] = '12,860' #13000 - len(message)
		postData['ctl00$mainContentPlaceHolder$lineCountDown'] = '94'
		postData['ctl00$mainContentPlaceHolder$addressBox$clearButton'] = None
		postData['ctl00$mainContentPlaceHolder$saveButton'] = None
		postData['ctl00$mainContentPlaceHolder$cmdCancel'] = None
		postData['ctl00$mainContentPlaceHolder$addressBox$clearButton'] = None
		postData['ctl00$mainContentPlaceHolder$addressBox$okButton'] = None
		postData[fieldName] = 'on'
		postData['ctl00$mainContentPlaceHolder$addressBox$addressStringTextBox'] = address
		postData['ctl00$topScriptManager'] = 'ctl00$topUpdatePanel|ctl00$mainContentPlaceHolder$sendMessageButton'
		postData['__ASYNCPOST'] = 'true'
		postData['DES_ScriptFileIDState'] = '0|1|2|4|6|11|12|15|16|18|20|21|22|23|24|35|39|41|42|44|46|47|48|49|54|55|56|57|58|59|60'
		postData['DES_Group'] = ''
		
		#util.writeDebugFile('postdata.txt', str(postData))
		
		response = self._doRequest(url, 'post', data = postData)
		if response == None :
			logger.error("CorrLinks.sendNewMessage: failed to send message")
			return False
		#util.writeDebugFile('header.txt', response.headers)
		#util.writeDebugFile('sendmessage.html', response.text)
		if not corrlinksparser.doesSendMessageSucceed(htmlparser.HtmlDocument(response.text)) :
			logger.error("CorrLinks.sendNewMessage: error response from CorrLinks. ")
			util.writeDebugFile('error_sendmessage.html', response.text)
			return False
		return True

	def findInmateNumber(self) :
		url = self._makeUrl('/RegisterInmate.aspx')

		response = self._doRequestWithoutLogin(url, 'get')
		if response == None :
			logger.error("CorrLinks.findInmateNumber: failed get page")
			return None
		document = htmlparser.HtmlDocument(response.text)
		inmateNumber = corrlinksparser.findInmateNumber(document)
		if inmateNumber == None :
			logger.error("CorrLinks.findInmateNumber: can't find inmate number")
			return None
		return inmateNumber

	def login(self) :
		if not self.doLogin() :
			return False
		return True

	def doLogin(self) :
		logger.debug("CorrLinks.login: start login email=%s" % (self._email))

		url = self._makeUrl('/Login.aspx?ReturnUrl=%2fDefault.aspx')
		
		response = self._doRequestWithoutLogin(url, 'get')
		if response == None :
			return False

		document = htmlparser.HtmlDocument(response.text)
		form = corrlinksparser.getLoginForm(document)
		if form == None :
			return False

		# Can't use post data from form.inputsToParams
		postData = {
			'ctl00$mainContentPlaceHolder$loginUserNameTextBox' : self._email,
			'ctl00$mainContentPlaceHolder$loginPasswordTextBox' : self._password,
			'ctl00$mainContentPlaceHolder$loginButton' : form.getInputValue('ctl00$mainContentPlaceHolder$loginButton'),
			'__COMPRESSEDVIEWSTATE' : form.getInputValue('__COMPRESSEDVIEWSTATE'),
			'__VIEWSTATE' : form.getInputValue('__VIEWSTATE'),
			'__EVENTVALIDATION' : form.getInputValue('__EVENTVALIDATION'),
		}
		response = self._doRequestWithoutLogin(url, 'post', postData)
		if response == None :
			return False
		
		#util.writeDebugFile('header.txt', response.headers)
		#util.writeDebugFile('login.html', response.text)
		
		document = htmlparser.HtmlDocument(response.text)
		solvedToken = self.solveRecaptcha(document, url)

		self._saveSession()
		
		if solvedToken == None :
			logger.error("CorrLinks.login: failed login email=%s" % (self._email))
			return False
		else :
			logger.debug("CorrLinks.login: succeeded login email=%s" % (self._email))
			return True

	def solveRecaptcha(self, document, returnUrl, postIt = True) :
		logger.debug("CorrLinks.solveRecaptcha: start solving recaptcha")

		twoCaptchaKey = '9f6fb7d647464e2f0885ff6dab7e2599'
		twoCaptchaSolverUrl = 'http://2captcha.com/in.php?key={0}&method=userrecaptcha&googlekey={1}&pageurl={2}&json=1'
		twoCaptchaResultUrl = 'http://2captcha.com/res.php?key={0}&action=get&json=1&id={1}'

		form = corrlinksparser.getRecaptchaForm(document)
		if form == None :
			return None

		googlekey = corrlinksparser.getRecaptchaSiteKey(document)
		if googlekey == None :
			return None
		solverUrl = twoCaptchaSolverUrl.format(twoCaptchaKey, googlekey, returnUrl)

		response = self._doRequestWithoutLogin(solverUrl, 'get')
		if response == None :
			return None

		solverResponse = util.decodeJson(response.text)
		if solverResponse == None or solverResponse['status'] == 0 :
			logger.error(
				"CorrLinks.solveRecaptcha: 2captcha responses wrong status for recaptcha submission: url=%s response=%s"
					% (solverUrl, response.text)
			)
			return None
		resultUrl = twoCaptchaResultUrl.format(twoCaptchaKey, solverResponse['request'])
		
		result = None
		
		while True :
			response = self._doRequestWithoutLogin(resultUrl, 'get')
			if response != None :
				result = util.decodeJson(response.text)
				if result != None :
					if result['status'] != 0 :
						break
					if result['request'] != 'CAPCHA_NOT_READY' :
						logger.error("CorrLinks.solveRecaptcha: 2captcha responses unhandled error code when getting result: %s" % (result['request']))
						return None
			util.sleepForSeconds(5)

		solvedToken = result['request']
		postData = form.inputsToParams()
		postData['g-recaptcha-response'] = solvedToken
		
		if postIt :
			response = self._doRequestWithoutLogin(returnUrl, 'post', postData)
			if response == None :
				return None
			#util.writeDebugFile('captcha.html', response.text)

		logger.debug("CorrLinks.solveRecaptcha: succeeded solving recaptcha")

		return solvedToken

