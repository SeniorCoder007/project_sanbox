import common.util as util
import common.logger as logger

import imaplib
import email

class MailFetcher :
	def __init__(self, email, password, server = None) :
		self._email = email
		self._password = password
		self._server = server
		if util.isEmptyText(self._server) :
			parsedEmail = util.splitEmailAddress(email)
			self._server = 'imap.' + parsedEmail['domain']
		self._imap = None
		self._hasLogined = False
		self._lastLoginTime = 0

	def connect(self) :
		self._hasLogined = False
		try :
			self._imap = imaplib.IMAP4_SSL(self._server)
		except :
			self._imap = None
			logger.error('MailFetcher: failed to connect to server %s' % (self._server))

	def disconnect(self) :
		self._hasLogined = False
		self.logout()
		self._imap = None
		
	def hasConnected(self) :
		return self._imap != None
		
	def getEmail(self) :
		return self._email

	def getFolder(self) :
		return 'inbox'

	def login(self) :
		self.disconnect()
		self._doCheckConnect()

		if not self.hasConnected() :
			return False
		
		logger.info('MailFetcher: login %s' % (self._email))
		try :
			if self._doCheckResult(self._imap.login(self._email, self._password)) == None :
				logger.error('MailFetcher: failed login %s' % (self._email))
				return False
			self._imap.list()
			self._imap.select("inbox")

			self._lastLoginTime = util.getCurrentTimestamp()
		except :
			logger.exception('MailFetcher: exception when login %s' % (self._email))
			return False
	
		self._hasLogined = True
		logger.info('MailFetcher: succeed login %s' % (self._email))
		
	def logout(self) :
		if not self.hasConnected() :
			return

		try :		
			self._imap.close()
			self._imap.logout()
		except :
			pass
			#logger.exception('MailFetcher.logout')
		
	def getUidsSinceDate(self, since) :
		if not self._doCheckLogin() :
			return []

		date = since.strftime("%d-%b-%Y")
		try :
			data = self._doCheckResult(self._imap.uid('search', None, '(SENTSINCE {date})'.format(date = date)))
			if len(data) < 1 :
				return []
			uidList = data[0].decode('utf-8').split()
		except :
			logger.exception('MailFetcher.getUidsSinceDate')
			self._doRequestReconnect()
			return []
		return uidList
		
	def fetchMessage(self, uid) :
		if not self._doCheckLogin() :
			return None

		rawMail = self._doFetchRawMail(uid)
		message = self._doParseRawMail(rawMail)
		if message != None :
			message['uid'] = uid
		return message
		
	def fetchMessageList(self, uidList) :
		if not self._doCheckLogin() :
			return []

		messageList = []
		for uid in uidList :
			rawMail = self._doFetchRawMail(uid)
			message = self._doParseRawMail(rawMail)
			if message != None :
				message['uid'] = uid
			messageList.append(message)
		return messageList
		
	def _doFetchRawMail(self, uid) :
		try :
			data = self._doCheckResult(self._imap.uid('fetch', uid, '(RFC822)'))
			if data == None :
				return None
			if len(data) < 1 or len(data[0]) < 2 :
				return None
			return data[0][1]
		except :
			logger.exception('MailFetcher._doFetchRawMail')
			self._doRequestReconnect()
			return None
		
	def _doParseRawMail(self, rawMail) :
		if rawMail == None :
			return None
		mail = email.message_from_bytes(rawMail)
		
		parsedTo = email.utils.parseaddr(mail['To'])
		parsedFrom = email.utils.parseaddr(mail['From'])
		result = {
			'contentMainType' : mail.get_content_maintype(),
			'contentType' : '',
			'to' : parsedTo[1],
			'from' : parsedFrom[1],
			'body' : '',
			'subject' : mail['Subject'],
			'messageId' : mail['message-id'],
		}
		
		if mail.is_multipart():
			for part in mail.get_payload():
				contentType = part.get_content_type()
				if contentType in [ 'text/plain', 'text/html' ] :
					result['contentType'] = contentType
					result['body'] = part.get_payload()
					break
		else:
			result['body'] = mail.get_payload()
			
		return result

	def _doCheckResult(self, result) :
		if len(result) < 2 :
			return None
		if result[0] == 'OK' :
			return result[1]
		return None

	def _doCheckLogin(self) :
		if not self._hasLogined :
			self.login()
		elif util.getCurrentTimestamp() - self._lastLoginTime >= 60 * 10 :
			self.login()
		return self._hasLogined

	def _doRequestReconnect(self) :
		self.disconnect()
		
	def _doCheckConnect(self) :
		if not self.hasConnected() :
			self.connect()
		return self.hasConnected()
