import threading

class ConditionEvent :
	def __init__(self) :
		self._lock = threading.RLock()
		self._condition = threading.Condition(self._lock)
		self._value = None
	
	def set(self, value = True):
		self._condition.acquire()
		self._value = value
		self._condition.notify()
		self._condition.release()

	def reset(self, value = None):
		self._condition.acquire()
		self._value = value
		self._condition.release()

	def wait(self, autoReset = True):
		self._condition.acquire()
		if not self._value :
			self._condition.wait(0.001)
		result = self._value
		if result != None and autoReset :
			self._value = None
		self._condition.release()
		return result

