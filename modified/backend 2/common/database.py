import common.util as util
import common.locker as locker
import common.logger as logger
import threading

import pymysql.cursors

class Cursor :
	def __init__(self, cursor, database) :
		self._cursor = cursor
		self._database = database

	def __enter__(self) :
		return self

	def __exit__(self, type, value, traceBack) :
		self.close()
		
	def close(self) :
		self._cursor.close()
		
	def execute(self, query, args = None) :
		with self._database.locker() :
			try :
				return self._cursor.execute(query, args)
			except :
				print('--------------- ' + query)
				print('--------------- ' + str(args))
				logger.exception('Cursor.execute')
				return None
		
	def insert(self, query, args = None) :
		with self._database.locker() :
			try :
				self._cursor.execute(query, args)
				return self._cursor.lastrowid
			except :
				print('--------------- ' + query)
				print('--------------- ' + str(args))
				logger.exception('Cursor.insert')
				return None
		
	def fetchOne(self) :
		with self._database.locker() :
			return self._cursor.fetchone()
	
	def fetchAll(self) :
		with self._database.locker() :
			return self._cursor.fetchall()

class TableLocker :
	def __init__(self, cursor, tableName) :
		self._cursor = cursor
		self._tableName = tableName

	def __enter__(self) :
		self._cursor.execute('lock tables %s write' % (self._tableName))
		return self

	def __exit__(self, type, value, traceBack) :
		self._cursor.execute('unlock tables')
		
		
class Transaction :
	def __init__(self, connection, database) :
		self._connection = connection
		self._database = database

	def __enter__(self) :
		self.begin()
		return self

	def __exit__(self, type, value, traceBack) :
		self.commit()
	
	def begin(self) :
		with self._database.locker() :
			self._connection.begin()

	def commit(self) :
		with self._database.locker() :
			self._connection.commit()
		
class Database :
	_instance = None
	
	@staticmethod
	def getDatabase() :
		return Database._instance

	def __init__(self, config) :
		Database._instance = self
		self._config = config
		self._connection = None
		self._lock = threading.Lock()
		self.open(config)
	
	# config: { "host" : "127.0.0.1", "port" : 3306, "database" : "", "user" : "", "password" : "" }
	def open(self, config) :
		self.close()

		self._connection = pymysql.connect(
			host = util.getDictValue(config, 'host', '127.0.0.1'),
			port = util.getDictValue(config, 'port', '3306'),
			user = util.getDictValue(config, 'user', ''),
			password = util.getDictValue(config, 'password', ''),
			db = util.getDictValue(config, 'database', ''),
			charset = 'utf8mb4',
			cursorclass = pymysql.cursors.DictCursor,
			autocommit  = True
		)
	
	def close(self) :
		if self._connection :
			self._connection.close()
			self._connection = None

	def beginTransaction(self) :
		self._connection.begin()

	def commitTransaction(self) :
		self._connection.commit()
		
	def cursor(self) :
		return Cursor(self._connection.cursor(), self)

	def transaction(self) :
		return Transaction(self._connection, self)

	def locker(self) :
		return locker.Locker(self._lock)
