import workers.baseworker as baseworker
import models.inmatemodel as inmatemodel
import models.phonenumbermodel as phonenumbermodel
import app.constants as constants
import app.apputil as apputil
import app.appconfig as appconfig
import common.util as util
import common.logger as logger

import datetime

class BalanceMonitor(baseworker.BaseWorker) :
	def __init__(self) :
		baseworker.BaseWorker.__init__(self)
		self._lastCheckedDay = 0

	def _doThreadRun(self) :
		self._executeBilling()
		while not self.shouldStop() :
			self.sleepForSeconds(60)
			self._checkExecuteBilling()

	def _checkExecuteBilling(self) :
		currentTime = datetime.datetime.now()
		if currentTime.day == self._lastCheckedDay :
			return
		if currentTime.hour != 23 :
			return
		if currentTime.minute <= 45 :
			return
		self._lastCheckedDay = currentTime.day
		self._executeBilling()

	def _executeBilling(self) :
		inmateModel = inmatemodel.InmateModel()
		inmateList = inmateModel.getAllInmates()
		for inmate in inmateList :
			apputil.checkBilling(inmate)
			self._doCheckRecylePhoneNumber(inmate)

	def _doCheckRecylePhoneNumber(self, inmate) :
		recyleTime = inmate[inmatemodel.InmateModel.column_service_expiry]
		recyleTime += util.daysToSeconds(appconfig.AppConfig.getConfig().getInmatePhoneRecycleDays())
		todayTime = util.calculateExpiryTime(util.getCurrentTimestamp(), 0)
		if recyleTime > todayTime :
			return
		phoneNumberModel = phonenumbermodel.PhoneNumberModel()
		inmateModel = inmatemodel.InmateModel()
		inmateModel.setPhoneNumber(inmate[inmateModel.column_inmate_number], '')
		phoneNumberModel.recyclePhoneNumber(inmate[inmateModel.column_inmate_number])
		logger.info('Recycled phone number. Inmate: %s, phone number : %s' % (inmate[inmateModel.column_inmate_number], inmate[inmateModel.column_phone_number]))
