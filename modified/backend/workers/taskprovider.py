import workers.baseworker as baseworker
import models.taskmodel as taskmodel
import common.util as util
import common.locker as locker

import threading


class TaskProvider(baseworker.BaseWorker) :
	def __init__(self) :
		baseworker.BaseWorker.__init__(self)
		self._taskModel = taskmodel.TaskModel()
		self._taskModel.resetProcessingTasks()
		self._fetchLock = threading.Lock()

	def fetchTask(self) :
		with locker.Locker(self._fetchLock) :
			task = None
			taskList = self._taskModel.getTasks(1)
			if len(taskList) > 0 :
				task = taskList[0]
				self._taskModel.beginTask(task)
				
			return task
		
	def completeTask(self, task) :
		self._taskModel.completeTask(task)

	def postponeTask(self, task) :
		self._taskModel.postponeTask(task)

	def addTask(self, task) :
		self._taskModel.addTask(task)
		
	def _doThreadRun(self) :
		while not self.shouldStop() :
			util.sleepForMilliseconds(100)

