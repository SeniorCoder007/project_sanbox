import workers.baseworker as baseworker
import common.util as util
import common.logger as logger
import common.mailfetcher as mailfetcher
import models.processedemailmodel as processedemailmodel
import models.emailaccountmodel as emailaccountmodel
import models.taskmodel as taskmodel
import app.constants as constants

import datetime
import re

class EmailAccountMonitor :
	def __init__(self, account, config) :
		self._account = account
		self._config = config
		self._mailFetcher = mailfetcher.MailFetcher(
			self._account['email'],
			self._account['password'],
			util.getDictValue(self._account, 'server')
		)
		self._fetchDays = 7
		self._processedEmailModel = processedemailmodel.ProcessedEmailModel()
		
	def execute(self) :
		try :
			self._doExecute()
		except :
			logger.exception('EmailAccountMonitor')

	def _doExecute(self) :
		fetchDays = self._fetchDays
		self._fetchDays = 2
		uidList = self._mailFetcher.getUidsSinceDate(datetime.date.today() - datetime.timedelta(days = fetchDays))
		uidList = self._processedEmailModel.getUnprocessedUidList(uidList, self._mailFetcher.getEmail(), self._mailFetcher.getFolder())
		for uid in uidList :
			message = self._mailFetcher.fetchMessage(uid)
			if message != None :
				self._doProcessMessage(message)
			self._processedEmailModel.processedEmail(uid, self._mailFetcher.getEmail(), self._mailFetcher.getFolder())
			
	def _doProcessMessage(self, message) :
		self._doProcessCorrLinksInvitation(message)

	def _doProcessCorrLinksInvitation(self, message) :
		isFromCorrLinks = util.emailAddressHasDomain(message['from'], 'corrlinks.com')
		if self._config.debugSkipCorrLinksEmailAddressCheck() :
			isFromCorrLinks = True
		if not isFromCorrLinks :
			logger.warning('EmailAccountMonitor._doProcessCorrLinksInvitation: email %s is not from CorrLinks' % (message['from']))
			return False
			
		body = util.base64Decode(message['body'])
		if body == None :
			body = message['body']
		matches = re.search(r'Email\s+Address\s*:.*?([\w\d\+\-_]+\@[\w\d\-]+(\.[\w\d]+)+).*?Identification\s+Code\s*:\s*([\w\d]+)', body, re.I | re.S)
		if matches != None :
			email = matches.group(1)
			code = matches.group(3)
			taskmodel.TaskModel().addTask({
				'type' : constants.TaskType.corrLinksInvitation,
				'content' : {
					'email' : email,
					'code' : code,
				}
			})
			return True

		logger.warning('EmailAccountMonitor._doProcessCorrLinksInvitation: can not find identification code from %s subject %s'
			% (message['from'], message['subject'])
		)

		return False

class EmailMonitor(baseworker.BaseWorker) :
	def __init__(self) :
		baseworker.BaseWorker.__init__(self)
		self._accountIndex = 0
		self._accountMonitorList = []
		accountList = emailaccountmodel.EmailAccountModel().getAllAccounts()
		for account in accountList :
			self._accountMonitorList.append(EmailAccountMonitor(
				account = account,
				config = self.getConfig()
			))

	def _doThreadRun(self) :
		while not self.shouldStop() :
			if self._accountIndex < len(self._accountMonitorList) :
				self._accountMonitorList[self._accountIndex].execute()
				util.sleepForMilliseconds(100)
			
			self._accountIndex += 1
			
			if self._accountIndex >= len(self._accountMonitorList) :
				self._accountIndex = 0
				self.sleepForSeconds(60 * 3)

			self.doHeartbeat()
