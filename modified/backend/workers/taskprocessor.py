import workers.baseworker as baseworker
import app.constants as constants
import app.apputil as apputil
import app.signalwire as signalwire
import models.taskmodel as taskmodel
import common.util as util
import common.logger as logger

from workers.taskhandlers.test import Test as TestHandler
from workers.taskhandlers.corrlinksinvitation import CorrLinksInvitation
from workers.taskhandlers.receivedmessagefromcorrlinks import ReceivedMessageFromCorrLinks
from workers.taskhandlers.receivedmessagefromsms import ReceivedMessageFromSms
from workers.taskhandlers.refillfund import RefillFund
from workers.taskhandlers.sendmail import SendMail
from workers.taskhandlers.sendcorrlinksmessage import SendCorrLinksMessage

class TaskProcessor(baseworker.BaseWorker) :
	def __init__(self, taskProvider) :
		baseworker.BaseWorker.__init__(self)
		self._taskProvider = taskProvider
		self._handlerMap = {
			constants.TaskType.test : TestHandler,
			constants.TaskType.corrLinksInvitation : CorrLinksInvitation,
			constants.TaskType.receivedMessageFromCorrLinks : ReceivedMessageFromCorrLinks,
			constants.TaskType.receivedMessageFromSms : ReceivedMessageFromSms,
			constants.TaskType.refillFund : RefillFund,
			constants.TaskType.sendMail : SendMail,
			constants.TaskType.sendCorrLinksMessage : SendCorrLinksMessage,
		}

	def _doThreadRun(self) :
		while not self.shouldStop() :
			task = self._taskProvider.fetchTask()
			if task != None :
				self._doProcessTask(task)
			else :
				util.sleepForSeconds(10)

	def _doProcessTask(self, task) :
		type = task[taskmodel.TaskModel.column_type]
		if type in self._handlerMap :
			handler = self._handlerMap[type]()
			handler.initialize(task, self._taskProvider)
			handler.process()
		else :
			logger.error("TaskProcessor: unknown task type %s" % (type))

