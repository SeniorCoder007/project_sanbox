import app.basethread as basethread
import app.application as application
import common.util as util
import common.logger as logger

class BaseWorker(basethread.BaseThread) :
	def __init__(self) :
		basethread.BaseThread.__init__(self)
		self._previousHeartbeatTime = 0
		
	def getConfig(self) :
		return application.Application.getApplication().getConfig()

	def doHeartbeat(self, name = None, interval = 60 * 5) :
		if interval <= 0 :
			interval = 60
		t = util.getCurrentTimestamp()
		if t - self._previousHeartbeatTime >= interval :
			self._previousHeartbeatTime = t
			if name == None :
				name = self.__class__.__name__
			logger.debug(name + ' is running')
