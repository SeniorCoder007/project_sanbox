import common.util as util

class TextLines :
	def __init__(self, fileName = None) :
		self._lines = []
		if fileName != None :
			self.loadFromFile(fileName)
	
	def loadFromFile(self, fileName) :
		with open(fileName) as f:
			lines = f.readlines()
		self._lines = []
		for line in lines :
			line = util.trim(line)
			if line != '' :
				self._lines.append(line)

	def getRandomLine(self) :
		if len(self._lines) == 0 :
			return ''
		return self._lines[util.getRandomInt(0, len(self._lines))]
