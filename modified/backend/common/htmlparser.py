from bs4 import BeautifulSoup

def _extractSoupList(resultList, soupList, classType) :
	if resultList == None : resultList = []

	for node in soupList :
		resultList.append(classType(node))

	return resultList


def _findTopLevelTags(resultList, soup, tagName) :
	if resultList == None : resultList = []
	if not 'find_all' in dir(soup) : return resultList

	nodeList = soup.find_all(tagName, recursive = False)
	if len(nodeList) > 0 :
		return resultList + nodeList;

	for node in soup.children :
		resultList = _findTopLevelTags(resultList, node, tagName)
	
	return resultList

class HtmlNode :
	def __init__(self, soup) :
		self._soup = soup
		for name in self._soup.attrs :
			self._soup.attrs[name.lower()] = self._soup.attrs[name]
		
	def __repr__(self) :
		return str(self.getTagName()) + ' ' + str(self._soup.attrs)

	def getTagName(self) :
		return self._soup.name

	def getAttr(self, name, defaultValue = None, keepList = True) :
		if name in self._soup.attrs :
			result = self._soup.attrs[name]
			if (not keepList) and isinstance(result, list) :
				if len(result) > 0 :
					return result[0]
				else :
					return ''

			return result
		else :
			return defaultValue

	def hasClass(self, cls) :
		return cls in self.getAttr('class', [])
		
	def findChildren(self, tagName, attrs = None, recursive = True, classType = None, **kwargs) :
		if classType == None :
			classType = HtmlNode
		return _extractSoupList([], self._soup.find_all(tagName, attrs = attrs, recursive = recursive, **kwargs), classType)
		
	def findFirstChild(self, tagName, attrs = None, recursive = True, classType = None, **kwargs) :
		children = self.findChildren(tagName, attrs, recursive, classType, **kwargs)
		if len(children) == 0 :
			return None
		return children[0]

	def getSoup(self) :
		return self._soup
		
	def getContentString(self) :
		content = self._soup.string
		if content == None :
			content = ''
		return content
	
	def getContentMultiStrings(self, delimiter = ' ') :
		textList = self._soup.stripped_strings
		content = delimiter.join(textList)
		return content.strip()

class HtmlInput(HtmlNode) :
	def __init__(self, node) :
		HtmlNode.__init__(self, node)

	def getName(self) :
		return self.getAttr('name', '')

	def getValue(self) :
		return self.getAttr('value', '')


class HtmlForm(HtmlNode) :
	def __init__(self, node) :
		HtmlNode.__init__(self, node)

		self._inputList = _extractSoupList([], node.find_all('input'), HtmlInput)
	
	def __repr__(self) :
		return HtmlNode.__repr__(self) + "\n" + str(self._inputList)

	def inputsToParams(self) :
		result = {}
		for input in self._inputList :
			name = input.getName()
			if name != '' :
				result[name] = input.getValue()

		return result
		
	def getInputValue(self, name) :
		for input in self._inputList :
			if name == input.getName() :
				return input.getValue()
		return ''

	def getInputList(self) :
		return self._inputList

class HtmlTableCell(HtmlNode) :
	def __init__(self, node) :
		HtmlNode.__init__(self, node)

	def isHeader(self) :
		return self.getTagName() == 'th'

class HtmlTableRow(HtmlNode) :
	def __init__(self, node) :
		HtmlNode.__init__(self, node)
		self._cellList = _extractSoupList([], _findTopLevelTags([], node, [ 'th', 'td' ]), HtmlTableCell)
	
	def __repr__(self) :
		return HtmlNode.__repr__(self) + "\n" + str(self._cellList)

	def getCellList(self) :
		return self._cellList

class HtmlTable(HtmlNode) :
	def __init__(self, node) :
		HtmlNode.__init__(self, node)
		self._rowList = _extractSoupList([], _findTopLevelTags([], node, 'tr'), HtmlTableRow)
	
	def getRowList(self) :
		return self._rowList
		
class HtmlDocument(HtmlNode) :
	def __init__(self, content) :
		self._content = content
		soup = BeautifulSoup(content, 'html.parser')
		HtmlNode.__init__(self, soup)

	def getFormList(self) :
		return _extractSoupList([], self._soup.find_all('form'), HtmlForm)

	def getTableList(self) :
		return _extractSoupList([], _findTopLevelTags([], self._soup, 'table'), HtmlTable)

	def getContent(self) :
		return self._content
