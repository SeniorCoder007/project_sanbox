import common.util as util

import logging
import time
import sys
 
from logging import StreamHandler
from logging.handlers import TimedRotatingFileHandler

_logger = None
_logHandler = None

def _createLogger(fileName = None, level = 'debug') :
	global _logger

	_logger = logging.getLogger('textinmate')
	_logger.setLevel(logging.INFO)
	setLogFile(None)
	
def setLogFile(fileName) :
	global _logHandler
	
	logger = getLogger()
	if _logHandler != None :
		logger.removeHandler(_logHandler)
		_logHandler = None

	if util.isEmptyText(fileName) :
		_logHandler = StreamHandler(sys.stdout)
	else :
		_logHandler = TimedRotatingFileHandler(fileName, when = "d", interval = 1, backupCount = 5)
	formatter = logging.Formatter('%(name)s %(asctime)s [%(process)d] %(levelname)s: %(message)s')
	formatter.converter = time.gmtime  # UTC time
	_logHandler.setFormatter(formatter)

	logger.addHandler(_logHandler)
	
def setLogLevel(level) :	
	loggingLevel = logging.INFO
	if level == 'fatal' or level == 'critical' :
		loggingLevel = logging.CRITICAL
	elif level == 'error' :
		loggingLevel = logging.ERROR
	elif level == 'warning' :
		loggingLevel = logging.WARNING
	elif level == 'info' :
		loggingLevel = logging.INFO
	elif level == 'debug' :
		loggingLevel = logging.DEBUG
	getLogger().setLevel(loggingLevel)

def getLogger() :
	global _logger
	if _logger == None :
		_createLogger()
	return _logger

def critical(message) :
	getLogger().critical(message)

def fatal(message) :
	getLogger().critical(message)

def error(message) :
	getLogger().error(message)

def warning(message) :
	getLogger().warning(message)

def info(message) :
	getLogger().info(message)

def debug(message) :
	getLogger().debug(message)

def exception(message) :
	getLogger().exception(message + " (caught exception, it's safe)")
