import app.applang as applang

def test_singleLine() :
	lang = applang.AppLang.getLang()
	assert lang.getText('test1') == 'this is test1 {data}'
	assert lang.getText('test1', { 'data': 'abc def' }) == 'this is test1 abc def'

def test_multipleLine() :
	lang = applang.AppLang.getLang()
	assert lang.getText('test2') == "first line\nsecond line {data}"
	assert lang.getText('test2', { 'data': 'hello world' }) == "first line\nsecond line hello world"
