import datetime

def makeTimestamp(year, month, day, hour = 0, minute = 0) :
	return datetime.datetime(year, month, day, hour, minute, 0).timestamp()

