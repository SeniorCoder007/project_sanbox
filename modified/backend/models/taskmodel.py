import models.basemodel as basemodel
import common.util as util
import common.logger as logger
import app.constants as constants

class TaskModel(basemodel.BaseModel) :
	table = 'task'
	
	column_id = 'id'
	column_type = 'type'
	column_status = 'status'
	column_content = 'content'
	column_time = 'time'
	column_execute_time = 'execute_time'
	
	
	def __init__(self) :
		basemodel.BaseModel.__init__(self)
		
	def resetProcessingTasks(self) :
		query = '''
			update {tableName}
			set {column_status} = %s
			where {column_status} = %s
		'''.format(
			tableName = self.table,
			column_status = self.column_status,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				int(constants.TaskStatus.open),
				int(constants.TaskStatus.processing),
			])

	def _logTask(self, name, task) :
		logger.debug("TaskModel.%s: task=%s"
			% (name, constants.TaskType(int(task['type'])).name))

	def addTask(self, task, executeTime = 0) :
		self._logTask('addTask', task)

		query = '''
			insert into {tableName}
			({column_type}, {column_status}, {column_content}, {column_time}, {column_execute_time})
			values (%s, %s, %s, %s, %s)
		'''.format(
			tableName = self.table,
			column_type = self.column_type,
			column_status = self.column_status,
			column_content = self.column_content,
			column_time = self.column_time,
			column_execute_time = self.column_execute_time,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				int(task['type']),
				int(constants.TaskStatus.open),
				util.encodeJson(task['content']),
				util.getCurrentTimestamp(),
				executeTime,
			])

	def getTasks(self, count = 1, executeTime = None, status = int(constants.TaskStatus.open)) :
		if executeTime == None :
			executeTime = util.getCurrentTimestamp()

		query = '''
			select * from {tableName}
			where {column_status} = {status}
			and {column_execute_time} <= {executeTime}
			order by {column_time} ASC
			limit 0, {count}
		'''.format(
			tableName = self.table,
			column_status = self.column_status,
			column_time = self.column_time,
			column_execute_time = self.column_execute_time,
			status = status,
			executeTime = executeTime,
			count = count,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query)
			rows = cursor.fetchAll()
			for row in rows :
				row[self.column_content] = util.decodeJson(row[self.column_content])
			return rows
			
	def beginTask(self, task) :
		self._logTask('beginTask', task)

		query = '''
			update {tableName}
			set {column_status} = %s
			where {column_id} = %s
		'''.format(
			tableName = self.table,
			column_status = self.column_status,
			column_id = self.column_id,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				int(constants.TaskStatus.processing),
				int(task[self.column_id]),
			])
		
	def completeTask(self, task) :
		self._logTask('completeTask', task)

		query = '''
			delete from {tableName}
			where {column_id} = %s
		'''.format(
			tableName = self.table,
			column_id = self.column_id,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				task[self.column_id]
			])
		
	def postponeTask(self, task) :
		self._logTask('postponeTask', task)

		query = '''
			update {tableName}
			set {column_status} = %s, {column_time} = %s, {column_execute_time} = %s
			where {column_id} = %s
		'''.format(
			tableName = self.table,
			column_status = self.column_status,
			column_time = self.column_time,
			column_execute_time = self.column_execute_time,
			column_id = self.column_id,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				int(constants.TaskStatus.open),
				util.getCurrentTimestamp(),
				util.getCurrentTimestamp() + util.minutesToSeconds(3),
				int(task[self.column_id]),
			])
		
