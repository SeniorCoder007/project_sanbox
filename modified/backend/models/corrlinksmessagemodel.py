import models.basemodel as basemodel
import common.util as util
import common.logger as logger
import app.constants as constants

class CorrLinksMessageModel(basemodel.BaseModel) :
	table = 'corrlinks_message'
	
	column_id = 'id'
	column_inmate_number = 'inmate_number'
	column_subject = 'subject'
	column_content = 'content'
	column_message_time = 'message_time'
	column_hash = 'hash'
	column_time = 'time'
	
	def __init__(self) :
		basemodel.BaseModel.__init__(self)
		
	def addMessage(self, inmateNumber, subject, content, messageTime, hash) :
		data = {}
		data[self.column_inmate_number] = inmateNumber
		data[self.column_subject] = subject
		data[self.column_content] = content
		data[self.column_message_time] = messageTime
		data[self.column_hash] = hash
		data[self.column_time] = util.getCurrentTimestamp()
		self.insert(self.table, data)

	def getMessageByHash(self, hash) :
		query = '''
			select * from {tableName}
			where {column_hash} = '{hash}'
		'''.format(
			tableName = self.table,
			column_hash = self.column_hash,
			hash = hash,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query)
			row = cursor.fetchOne()
			return row
		return None

