import common.database as database

class BaseModel :
	def __init__(self) :
		pass

	def getDatabase(self) :
		return database.Database.getDatabase()

	def insert(self, tableName, data) :
		columnText = ''
		valueText = ''
		valueList = []
		for column in data :
			if columnText != '' :
				columnText += ','
				valueText += ','
			columnText += column
			valueText += '%s'
			valueList.append(data[column])
		query = '''
			insert into {tableName}
			({columnText})
			values ({valueText})
		'''.format(
			tableName = tableName,
			columnText = columnText,
			valueText = valueText,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			return cursor.insert(query, valueList)

	def update(self, tableName, data, where, columns = None) :
		whereData = self._doMakeWhereClause(where)
		set = ''
		params = []
		for key in data :
			if columns != None :
				if not (key in columns) :
					continue
			if len(set) > 0 :
				set += ', '
			set += key + ' = %s'
			params.append(data[key])
		query = '''
			update {tableName}
			set {set}
			{where}
		'''.format(
			tableName = tableName,
			set = set,
			where = whereData['clause'],
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, params + whereData['params'])

	def delete(self, tableName, where) :
		whereData = self._doMakeWhereClause(where)
		query = '''
			delete from {tableName}
			{where}
		'''.format(
			tableName = tableName,
			where = whereData['clause'],
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, whereData['params'])

	def _doMakeWhereClause(self, data) :
		if data == None :
			data = {}
		params = []
		query = ''
		for key in data :
			if len(params) > 0 :
				query += ' and '
			query += key + ' = %s'
			params.append(data[key])
		full = ''
		if len(params) > 0 :
			full = 'where ' + query
		return {
			'clause' : full,
			'expression' : query,
			'params' : params
		}
