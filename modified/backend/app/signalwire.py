from signalwire.rest import Client as signalwire_client
import app.constants as constants
import app.apputil as apputil
import common.logger as logger
import common.util as util

class SignalWire :
	_instance = None
	
	@staticmethod
	def getSignalWire() :
		return SignalWire._instance

	def __init__(self, projectKey, token, spaceUrl) :
		SignalWire._instance = self
		self._client = signalwire_client(projectKey, token, signalwire_space_url = spaceUrl)
		
	def sendMessage(self, fromPhoneNumber, toPhoneNumber, message) :
		fromPhoneNumber = apputil.normalizeUsPhoneNumber(fromPhoneNumber)
		toPhoneNumber = apputil.normalizeUsPhoneNumber(toPhoneNumber)
		try :
			response = self._client.messages.create(
				from_ = fromPhoneNumber,
				to = toPhoneNumber,
				body = message,
			)
			#print('====================')
			#print(response.__dict__)
			if response == None :
				return False
		except BaseException as e:
			logger.exception('SignalWire.sendMessage')
			message = str(e)
			if message.find('To invalid format') >= 0 : #invalid target phone number
				return True
			return False
		return True

	def getMessageList(self, since = None) :
		messageList = []
		pageSize = 1000
		try :
			response = self._client.messages.page(page_number = 0, page_size = pageSize, date_sent_after = since)
			if response == None :
				return messageList

			for m in response :
				message = self._doConvertSignalWireMessage(m)
				messageList.append(message)

			while response != None :
				response = response.next_page()
				if response == None :
					break
				for m in response :
					message = self._doConvertSignalWireMessage(m)
					messageList.append(message)
		except :
			logger.exception('SignalWire.getMessageList')

		return messageList

	def _doConvertDirection(self, direction) :
		if util.containsText(direction, 'inbound') :
			return constants.MessageDirection.inbound
		if util.containsText(direction, 'outbound') :
			return constants.MessageDirection.outbound
		return constants.MessageDirection.unknown

	def _doConvertSignalWireMessage(self, m) :
		message = {
			'uid' : m.sid,
			'fromPhoneNumber' : m.from_,
			'toPhoneNumber' : m.to,
			'direction' : int(self._doConvertDirection(m.direction)),
			'message' : m.body,
			'createTime' : util.dateTimeToTimestamp(m.date_created),
			'sentTime' : util.dateTimeToTimestamp(m.date_sent),
			'errorCode' : m.error_code,
			'errorMessage' : m.error_message,
		}
		
		return message
