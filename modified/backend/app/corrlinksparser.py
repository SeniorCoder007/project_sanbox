import common.htmlparser as htmlparser
import common.logger as logger
import common.util as util
import app.apputil as apputil

from datetime import datetime

import re


def getRegisterForm(document) :
	formList = document.getFormList()
	if len(formList) == 0 :
		logger.error("CorrLinksParser: Can't find register form")
		return None
	form = formList[0]
	return form

def getLoginForm(document) :
	formList = document.getFormList()
	if len(formList) == 0 :
		logger.error("CorrLinksParser: Can't find login form")
		return None
	form = formList[0]
	return form

def getRecaptchaSiteKey(document) :
	matches = re.search(r'\sdata-sitekey="(.*?)"', document.getContent())
	if matches == None :
		logger.error("CorrLinksParser: Can't find recaptcha sitekey")
		return None

	return matches.group(1)

def getRecaptchaForm(document) :
	formList = document.getFormList()
	if len(formList) == 0 :
		logger.error("CorrLinksParser: Can't find recaptcha form")
		return None
	form = formList[0]
	return form

def getInboxForm(document) :
	formList = document.getFormList()
	if len(formList) == 0 :
		logger.error("CorrLinksParser: Can't find inbox form")
		return None
	form = formList[0]
	return form

def getSendMessageForm(document) :
	formList = document.getFormList()
	if len(formList) == 0 :
		logger.error("CorrLinksParser: Can't find send message form")
		return None
	form = formList[0]
	return form

def getInboxItems(document) :
	tableList = document.getTableList()
	table = None
	for t in tableList :
		if t.hasClass('MessageDataGrid') :
			table = t
			break
	if table == None :
		return None
		
	itemList = []

	for row in table.getRowList() :
		onclick = row.getAttr('onclick')
		if util.isEmptyText(onclick) :
			continue
		if not util.containsText(onclick, '__doPostBack') :
			continue

		actionText = re.sub(r'__doPostBack\((.*)\)', r'\1', onclick)
		parts = actionText.split(',')
		if len(parts) != 2 :
			continue
		action = {
			'target' : re.sub(r"'", '', util.trim(parts[0])),
			'argument' : re.sub(r"'", '', util.trim(parts[1])),
		}
		
		cellList = row.getCellList()
		if len(cellList) < 4 :
			continue

		sender = ''
		senderNode = cellList[1].findFirstChild('span')
		if senderNode != None :
			sender = senderNode.getContentString()

		subject = ''
		subjectNode = cellList[2].findFirstChild('span')
		if subjectNode != None :
			subject = subjectNode.getContentString()

		date = textToDateTime(cellList[3].getContentString())
		date = date.timestamp()

		readLinkNode = cellList[4].findFirstChild('a')
		if readLinkNode != None :
			id = readLinkNode.getAttr('id')
			if id :
				parts = id.split('_')
				if len(parts) > 2 and parts[-1] == 'readLink' :
					readLink = parts[-2]

		itemList.append({
			'sender' : sender,
			'subject' : subject,
			'date' : date,
			'action' : action,
			'readLink' : readLink,
		})
	return itemList

def getInboxMessage(document) :
	message = {
		'sender' : '',
		'subject' : '',
		'date' : util.getCurrentTimestamp(),
		'message' : '',
	}

	node = document.findFirstChild('input', { 'name' : 'ctl00$mainContentPlaceHolder$fromTextBox' })
	if node != None :
		message['sender'] = node.getAttr('value')

	node = document.findFirstChild('input', { 'name' : 'ctl00$mainContentPlaceHolder$dateTextBox' })
	if node != None :
		message['date'] = textToDateTime(node.getAttr('value'))
		message['date'] = message['date'].timestamp()

	node = document.findFirstChild('input', { 'name' : 'ctl00$mainContentPlaceHolder$subjectTextBox' })
	if node != None :
		message['subject'] = node.getAttr('value')

	node = document.findFirstChild('textarea', { 'name' : 'ctl00$mainContentPlaceHolder$messageTextBox' })
	if node != None :
		message['message'] = node.getContentString()

	return message
	
def getSendMessageAddressBoxFieldName(document, inmateNumber) :
	table = document.findFirstChild('table', { 'class' : 'AddressBoxDataGrid' }, classType = htmlparser.HtmlTable)
	if table == None :
		return None
		
	for row in table.getRowList() :
		classes = row.getAttr('class')
		if classes != None and 'fixedAddressBoxHeaderTable' in classes :
			continue
		cellList = row.getCellList()
		if len(cellList) < 4 :
			continue

		found = False
		if isReceiverAddress(cellList[1].getContentString(), inmateNumber) :
			found = True
			
		if found :
			checkBox = cellList[0].findFirstChild('input')
			if checkBox != None :
				return checkBox.getAttr('name')
	return None

def findInmateNumber(document) :
	tableList = document.getTableList()
	for table in tableList :
		if not table.hasClass('MessageDataGrid') :
			continue
		rowList = table.getRowList()
		if len(rowList) < 2 :
			continue
		row = rowList[1]
		cellList = row.getCellList()
		if len(cellList) < 4 :
			continue
		inmateNumber = cellList[1].getContentString()
		inmateNumber = apputil.normalizeInmateNumber(inmateNumber)
		if len(inmateNumber) != 8 :
			return None
		return inmateNumber
	return None

def isReceiverAddress(text, inmateNumber) :
	matches = re.search(r'\(\s*(\d+)\s*\)', text)
	if matches == None :
		return False
	return matches.group(1) == inmateNumber
	
def getSendMessageReceiverAddress(document) :
	node = document.findFirstChild('input', { 'name' : 'ctl00$mainContentPlaceHolder$addressBox$addressStringTextBox' })
	if node == None :
		return None
	return node.getAttr('value')
	
def doesSendMessageSucceed(document) :
	return util.containsText(document.getContent(), 'MessageProcessed')

def textToDateTime(text) :
	try :
		return datetime.strptime(text, '%m/%d/%Y %I:%M:%S %p')
	except :
		return datetime.now()

def makeHtmlFromPiece(text) :
	text = re.sub(r'^.*?(<div>.*</div>).*?$', r'\1', text, flags = re.DOTALL)
	text = '<html><head></head><body>' + text + '</body></html>'
	return text
