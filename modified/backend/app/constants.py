import enum


@enum.unique
class InmatePaymentStatus(enum.IntEnum) :
	unpaid = 1 # neither paid nor in trial
	trial = 2
	paid = 3

# DON'T change any values below
# the values are stored in the database

@enum.unique
class TaskStatus(enum.IntEnum) :
	open = 1
	processing = 2

@enum.unique
class TaskType(enum.IntEnum) :
	test = 100
	receivedMessageFromCorrLinks = 1
	receivedMessageFromSms = 2
	corrLinksInvitation = 3
	refillFund = 4
	sendMail = 5
	sendCorrLinksMessage = 6

@enum.unique
class MessageDirection(enum.IntEnum) :
	inbound = 1
	outbound = 2
	unknown = 3
