import common.util as util
import common.logger as logger
import app.appconfig as appconfig
import app.constants as constants
import app.applang as applang
import models.inmatemodel as inmatemodel
import models.emailaccountmodel as emailaccountmodel
import models.phonenumbermodel as phonenumbermodel
import models.taskmodel as taskmodel

import datetime
import calendar
import re
import hashlib
import smtplib
from email.mime.text import MIMEText


def dollarToCredit(amount) :
	return float(amount) * 10000

def creditToDollar(amount) :
	return float(amount) / 10000

def normalizeUsPhoneNumber(number) :
	number = re.sub(r'[^\d]', '', number)
	if len(number) <= 10 :
		number = '1' + number
	if len(number) <= 11 :
		number = '+' + number
	return number

def removeAreaCodeFromUsPhoneNumber(number) :
	if len(number) == 12 :
		return number[2:]
	if len(number) == 11 :
		return number[1:]
	return number

def getInmateNumberFromSender(sender) :
	matches = re.search(r'\(\s*(\d+)\s*\)', sender)
	if matches == None :
		return None
	inmateNumber = matches.group(1)
	return inmateNumber

def getInmateNumberFromEmailAddress(email) :
	matches = re.search(r'\+([\d\-]+)\@', email)
	if matches == None :
		return None
	inmateNumber = matches.group(1)
	inmateNumber = normalizeInmateNumber(inmateNumber)
	if len(inmateNumber) != 8 :
		return None
	return inmateNumber

def normalizeInmateNumber(inmateNumber) :
	inmateNumber = re.sub(r'[^\d]', '', inmateNumber)
	return inmateNumber

def isValidInmateNumber(inmateNumber) :
	return inmateNumber != None and len(inmateNumber) == 8 and inmateNumber.isdigit()

def extractPhoneNumber(text, normalize = True) :
	matches = re.search(r'#(\+?[\d\- ]+)', text, re.S)
	if matches == None :
		return None
	number = matches.group(1)
	index = matches.start(1)
	if normalize :
		number = normalizeUsPhoneNumber(number)
	return {
		'number' : number,
		'index' : index,
	}

def extractSupport(text) :
	matches = re.search(r'#(support)', text, re.S)
	if matches == None :
		return None
	index = matches.start(1)
	return {
		'index' : index,
	}

def splitTextAtWordBoundary(text, length) :
	result = []
	while True :
		text = util.trim(text)
		if len(text) <= length :
			result.append(text)
			break
		index = length
		while index >= 0 and not text[index].isspace() :
			index -= 1
		if index < 0 :
			index = length
		sub = text[0 : index]
		text = text[index + 1:]
		result.append(util.trim(sub))
	return result

def removePreviousMessage(text) :
	text = re.sub(r'\-\-\-.*wrote\s*\:.*', '', text, flags = re.S | re.I)
	return text

def sendMail(to, subject, message) :
	smtpConfig = appconfig.AppConfig.getConfig().getSmtp()
	mail = MIMEText(message, 'plain')
	mail['From'] = smtpConfig['account']
	mail['To'] = to
	mail['Subject'] = subject
	try :
		if smtpConfig['port'] == 25 :
			smtp = smtplib.SMTP(smtpConfig['server'], smtpConfig['port'])
		else :
			smtp = smtplib.SMTP_SSL(smtpConfig['server'], smtpConfig['port'])
		smtp.ehlo()
		smtp.login(smtpConfig['account'], smtpConfig['password'])
		#smtp.sendmail(smtpConfig['account'], to, mail.as_string())
		smtp.close()

		return True
	except :
		logger.exception('apptuil.sendMail')
		return False

def getNewEmailAddress(inmateNumber) :
	emailAccountModel = emailaccountmodel.EmailAccountModel()
	#inmateModel = inmatemodel.InmateModel()
	emailAccount = emailAccountModel.getRandomAccount()
	if emailAccount == None :
		return ''
	sampleAddress = emailAccount[emailAccountModel.column_email]
	address = sampleAddress
	address = address.replace('@', '+' + inmateNumber + '@')
	return address
	'''
	while True :
		address = sampleAddress
		address = address.replace('@', '+' + util.getRandomString(8) + '@')
		if inmateModel.getInmateByEmail(address) == None :
			return address
	'''

def createInmateAccount(inmateNumber, allowTrial, email = None) :
	inmateModel = inmatemodel.InmateModel()
	currentTime = util.getCurrentTimestamp()
	if email == None :
		email = getNewEmailAddress(inmateNumber)
	data = {}
	data[inmateModel.column_email] = email
	if isValidInmateNumber(inmateNumber) :
		data[inmateModel.column_inmate_number] = inmateNumber
		data[inmateModel.column_phone_number] = phonenumbermodel.PhoneNumberModel().usePhoneNumber(inmateNumber)
	else :
		data[inmateModel.column_inmate_number] = ''
		data[inmateModel.column_phone_number] = ''
	data[inmateModel.column_credit] = 0
	
	data[inmateModel.column_create_time] = currentTime
	if allowTrial :
		data[inmateModel.column_trial_expiry] = util.calculateExpiryTime(currentTime, appconfig.AppConfig.getConfig().getInmateTrialDays())
		data[inmateModel.column_service_expiry] = data[inmateModel.column_trial_expiry]
	else :
		data[inmateModel.column_trial_expiry] = 0
		data[inmateModel.column_service_expiry] = util.calculateExpiryTime(currentTime, 0)
	data[inmateModel.column_billing_day] = util.getDayOfTimestamp(data[inmateModel.column_service_expiry])

	data[inmateModel.column_corrlinks_password] = ''
	data[inmateModel.column_corrlinks_first_name] = appconfig.AppConfig.getConfig().getFirstNameDict().getRandomLine()
	data[inmateModel.column_corrlinks_last_name] = appconfig.AppConfig.getConfig().getLastNameDict().getRandomLine()
	data[inmateModel.column_corrlinks_session] = ''
	
	id = inmateModel.insert(inmateModel.table, data)
	data[inmateModel.column_id] = id

	return data

def getInmatePaymentStatus(inmate, currentTimestamp = None) :
	if currentTimestamp == None :
		currentTimestamp = util.getCurrentTimestamp()

	if inmate == None :
		return constants.InmatePaymentStatus.unpaid

	if (currentTimestamp > inmate[inmatemodel.InmateModel.column_trial_expiry]
		and currentTimestamp <= inmate[inmatemodel.InmateModel.column_service_expiry]) :
		return constants.InmatePaymentStatus.paid

	if currentTimestamp <= inmate[inmatemodel.InmateModel.column_trial_expiry] :
		if (inmate[inmatemodel.InmateModel.column_service_expiry] > inmate[inmatemodel.InmateModel.column_trial_expiry]
			or inmate[inmatemodel.InmateModel.column_credit] > 0) :
			return constants.InmatePaymentStatus.paid
		return constants.InmatePaymentStatus.trial

	return constants.InmatePaymentStatus.unpaid

def isInmateCorrLinksRegistered(inmate) :
	return inmate[inmatemodel.InmateModel.column_corrlinks_password] != ''

def isServiceDisabled(inmate) :
	return getInmatePaymentStatus(inmate) == constants.InmatePaymentStatus.unpaid

def computeBilling(inmate, currentTimestamp = None, monthlyFee = None) :
	if currentTimestamp == None :
		currentTimestamp = util.getCurrentTimestamp()
	if monthlyFee == None :
		monthlyFee = appconfig.AppConfig.getConfig().getInmateMonthlyFee()

	inmateModel = inmatemodel.InmateModel()

	if inmate[inmateModel.column_credit] < monthlyFee :
		return None

	today = datetime.datetime.fromtimestamp(currentTimestamp)
	todayExpiry = util.calculateExpiryTime(currentTimestamp, 0)

	if inmate[inmateModel.column_service_expiry] > todayExpiry :
		return None

	previousStatus = getInmatePaymentStatus(inmate, currentTimestamp)

	newMonth = today.month + 1
	newYear = today.year
	if newMonth > 12 :
		newMonth = 1
		newYear += 1

	newDay = today.day
	newDaysInMonth = calendar.monthrange(newYear, newMonth)[1]
	if newDay > newDaysInMonth :
		newDay = newDaysInMonth

	billingDay = inmate[inmateModel.column_billing_day]
	if previousStatus == constants.InmatePaymentStatus.unpaid :
		billingDay = today.day

	if newDay < billingDay and billingDay <= newDaysInMonth :
		newDay = billingDay

	newExpiry = datetime.datetime(newYear, newMonth, newDay, 0, 0, 0).timestamp() + util.daysToSeconds(1) - 1

	result = {}
	result['monthlyFee'] = monthlyFee
	result[inmateModel.column_service_expiry] = newExpiry
	result[inmateModel.column_billing_day] = billingDay
	return result

def checkBilling(inmate, currentTimestamp = None, monthlyFee = None) :
	result = computeBilling(inmate, currentTimestamp, monthlyFee)
	if result == None :
		return False

	inmateModel = inmatemodel.InmateModel()
	inmateModel.executeBilling(
		inmate[inmateModel.column_inmate_number],
		result['monthlyFee'],
		result[inmateModel.column_service_expiry],
		result[inmateModel.column_billing_day]
	)
	inmate[inmateModel.column_service_expiry] = result[inmateModel.column_service_expiry]
	inmate[inmateModel.column_billing_day] = result[inmateModel.column_billing_day]
	logger.info('Billed %s' % (inmate[inmateModel.column_inmate_number]))
	return True

def checkAddInmatePhoneNumber(inmate, currentTimestamp = None) :
	inmateModel = inmatemodel.InmateModel()
	if not util.isEmptyText(inmate[inmateModel.column_phone_number]) :
		return
	status = getInmatePaymentStatus(inmate)
	if status == constants.InmatePaymentStatus.unpaid :
		return
	inmateNumber = inmate[inmateModel.column_inmate_number]
	phoneNumber = phonenumbermodel.PhoneNumberModel().usePhoneNumber(inmateNumber)
	inmateModel.setPhoneNumber(inmateNumber, phoneNumber)
	logger.info('Added phone number to inmate: %s, phone number : %s' % (inmateNumber, phoneNumber))

def formatSmsMessage(inmate, text) :
	status = getInmatePaymentStatus(inmate)
	key = None
	if status == constants.InmatePaymentStatus.trial :
		key = 'message.inTrialToSms'
	if status == constants.InmatePaymentStatus.unpaid :
		key = 'message.unpaid'
	if key != None :
		text = applang.AppLang.getLang().getText(key, {
			'message' : text,
			'url' : appconfig.AppConfig.getConfig().getUrlWebsite(),
			'inmateNumber' : inmate[inmatemodel.InmateModel.column_inmate_number],
			'trialExpiry' : formatExpiryTime(inmate[inmatemodel.InmateModel.column_trial_expiry]),
		})
	return text

def formatCorrLinksMessage(inmate, text) :
	status = getInmatePaymentStatus(inmate)
	key = None
	if status == constants.InmatePaymentStatus.trial :
		key = 'message.inTrialToCorrLinks'
	if status == constants.InmatePaymentStatus.unpaid :
		key = 'message.unpaid'
	if key != None :
		text = applang.AppLang.getLang().getText(key, {
			'message' : text,
			'url' : appconfig.AppConfig.getConfig().getUrlWebsite(),
			'inmateNumber' : inmate[inmatemodel.InmateModel.column_inmate_number],
			'trialExpiry' : formatExpiryTime(inmate[inmatemodel.InmateModel.column_trial_expiry]),
		})
	return text

def formatExpiryTime(expiryTimestamp) :
	return util.timestampToDateTime(expiryTimestamp, datetime.datetime.now()).strftime("%B %d, %Y")

def sendInmateWelcomeEmail(inmateNumber) :
	inmateModel = inmatemodel.InmateModel()
	inmate = inmateModel.getInmateByInmateNumber(inmateNumber)
	if inmate == None :
		logger.error("sendInmateWelcomeEmail: can't find inmate data")
		return

	paymentStatus = getInmatePaymentStatus(inmate)
	isInTrial = (paymentStatus == constants.InmatePaymentStatus.trial)

	langParams = {
		'phoneNumber' : removeAreaCodeFromUsPhoneNumber(inmate[inmateModel.column_phone_number]),
		'trialExpiry' : formatExpiryTime(inmate[inmateModel.column_trial_expiry]),
		'balance' : 'USD $%.2f' % (creditToDollar(inmate[inmateModel.column_credit])),
	}
	trialA = ''
	trialB = ''
	if isInTrial :
		trialA = applang.AppLang.getLang().getText('message.inmateWelcomeEmailBody.trialA', langParams)
		trialB = applang.AppLang.getLang().getText('message.inmateWelcomeEmailBody.trialB', langParams)
	langParams['trialA'] = trialA
	langParams['trialB'] = trialB
	subject = applang.AppLang.getLang().getText('message.inmateWelcomeEmailSubject', langParams)
	body = applang.AppLang.getLang().getText('message.inmateWelcomeEmailBody', langParams)

	#print(body)
	taskmodel.TaskModel().addTask({
			'type' : constants.TaskType.sendCorrLinksMessage,
			'content' : {
				'inmateNumber' : inmateNumber,
				'subject' : subject,
				'message' : body,
			},
	}, util.getCurrentTimestamp() + util.minutesToSeconds(90))

def canCheckCorrLinksMessages(inmate) :
	if not isInmateCorrLinksRegistered(inmate) :
		return False
	if isServiceDisabled(inmate) :
		now = datetime.datetime.now()
		if now.minute < 30 and now.hour == 2 :
			return True
		else :
			return False
	return True

def hashCorrLinksMessage(inmateNumber, subject, content) :
	message = util.trim(inmateNumber) + util.trim(subject) + util.trim(content)
	message = message.encode('utf-8')
	m = hashlib.sha256()
	m.update(message)
	return m.hexdigest()

