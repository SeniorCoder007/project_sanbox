import common.util as util
import common.logger as logger

class AppLang :
	_instance = None
	
	@staticmethod
	def getLang() :
		if AppLang._instance == None :
			AppLang._instance = AppLang()
		return AppLang._instance

	def __init__(self) :
		AppLang._instance = self

		self._langFileName = 'data/lang.json'
		self._lang = None
		self._doLoadLang()

	def _doLoadLang(self) :
		self._lang = util.decodeJson(util.readFileContent(self._langFileName))
		if self._lang == None :
			logger.error("Can't load language file!")

	def getText(self, key, params = None) :
		text = ''
		if key in self._lang :
			text = self._lang[key]

		if not util.isString(text) :
			text = "\n".join(text)

		if params != None :
			text = text.format(**params)
		return text
		

