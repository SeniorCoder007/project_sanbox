import workers.taskprovider as taskprovider
import workers.taskprocessor as taskprocessor
import workers.emailmonitor as emailmonitor
import workers.corrlinksmonitor as corrlinksmonitor
import workers.smsmonitor as smsmonitor
import workers.balancemonitor as balancemonitor

def initApplication(app) :
	taskProvider = app.appendWorker(taskprovider.TaskProvider())
	for _ in range(app.getConfig().getTaskProcessorCount()) :
		app.appendWorker(taskprocessor.TaskProcessor(taskProvider))

	app.appendWorker(emailmonitor.EmailMonitor())
	app.appendWorker(smsmonitor.SmsMonitor())
	app.appendWorker(balancemonitor.BalanceMonitor())

	corrLinksMonitorCount = app.getConfig().getCorrLinksMonitorCount()
	for i in range(corrLinksMonitorCount) :
		app.appendWorker(corrlinksmonitor.CorrLinksMonitor(corrLinksMonitorCount, i))



