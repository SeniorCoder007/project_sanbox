from common.conditionevent import ConditionEvent
import common.logger as logger
import common.util as util

import threading
import time


class BaseThread :
	def __init__(self) :
		self._stopEvent = ConditionEvent()
		self._finishedEvent = ConditionEvent()
		self._isRunning = False
		self._lock = threading.RLock()
		
	def run(self) :
		self._isRunning = True
		
		thread = threading.Thread(target = lambda : self._executeThread())
		thread.daemon = True
		thread.start()
		
	def stop(self) :
		self._stopEvent.set()
		
	def isRunning(self) :
		return self._isRunning
		
	def shouldStop(self) :
		return self._stopEvent.wait() != None
		
	def doFinished(self) :
		self._isRunning = False
		self._finishedEvent.set()

	def _executeThread(self) :
		try :
			self._doThreadRun()
		except Exception :
			logger.exception('Unhandled exception in worker thread')
		finally :
			if self._isRunning :
				self.doFinished()

	def _doThreadRun(self) :
		pass

	def sleepForSeconds(self, seconds) :
		t = util.getCurrentTimestamp()
		while not self.shouldStop() :
			if util.getCurrentTimestamp() - t >= seconds :
				break
			util.sleepForMilliseconds(100)
