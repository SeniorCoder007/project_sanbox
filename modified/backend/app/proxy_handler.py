import requests as req
import random
import time
import common.logger as logger



class Proxy_handler:
    def __init__(self):
        self.proxies = []
        self.username = 'lum-customer-hl_2c924221-zone-zone1'
        self.password = 'idrx4w6pqq1s'
        self.port = 22225
        self.fail_counter = 0
        self.headers = {
        'user-agent' : 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0',
    }
	
    def increment_counter(self):
        try:
            self.fail_counter += 1
        except Exception as e:
            logger.error('increment_counter: Error incrementing the counter.ß')

    def reset_counter(self):
        try:
            self.fail_counter = 0
        except Exception as e:
            logger.error('Error: reseting counter')

    def set_new_proxy(self): # this function to create a new session with a new proxy
        try:
            try:
                # check if our proxy list is empty and renew if it's true
                if len(self.proxies) == 0: 
                    self.renew_proxies()
                else:
                    pass
                # getting and setting a new random proxy
                if self.fail_counter < 2:
	                proxy = random.choice(self.proxies) # getting random proxies
	                self.proxies.remove(proxy) # remove the used proxy from the proxies list
	                self.sess_proxy = {'http': 'socks5h://'+proxy,'https': 'socks5h://'+proxy} #socks5 proxies
                else:
	                session_id = random.random()
	                proxy = ('http://%s-session-%s:%s@zproxy.luminati.io:%d' %
	                    (self.username, session_id, self.password, self.port))
	                self.sess_proxy = {'http':proxy,'https': proxy} #http proxies

            except:
                session_id = random.random()
                proxy = ('http://%s-session-%s:%s@zproxy.luminati.io:%d' %
                    (self.username, session_id, self.password, self.port))
                self.sess_proxy = {'http':proxy,'https': proxy} #http proxies


            return self.sess_proxy
            
        except Exception as e:
            logger.error('set_new_proxy: Error getting new random proxy.')

    def renew_proxies(self):
        try:
            resp = req.get('http://proxylist.online/proxy.php?key=4xq4ZKqGIRwBOnL4DvQwBplo0CrboEsX')
            self.proxies = resp.content.decode().replace('\n','').split('\r')[:-1]
        except Exception as e:
            logger.error('proxy_handler: Error renewing_proxies')