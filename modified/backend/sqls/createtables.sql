SET NAMES utf8mb4;

START TRANSACTION;

CREATE TABLE IF NOT EXISTS task
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	type TINYINT UNSIGNED NOT NULL DEFAULT 0,
	status TINYINT UNSIGNED NOT NULL DEFAULT 0,
	content TEXT NOT NULL,
	time INT UNSIGNED NOT NULL DEFAULT 0,
	execute_time INT UNSIGNED NOT NULL DEFAULT 0,

	PRIMARY KEY (id),
	INDEX (type),
	INDEX (status),
	INDEX (time),
	INDEX (execute_time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELIMITER //
DROP PROCEDURE IF EXISTS alterTask //
CREATE PROCEDURE alterTask()
BEGIN
	IF NOT EXISTS(
		SELECT NULL
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE table_name = 'task'
		/*AND table_schema = 'smsinside'*/
		AND column_name = 'execute_time'
	)
	THEN
		ALTER TABLE `task` ADD `execute_time` INT UNSIGNED NOT NULL default 0;
		ALTER TABLE `task` ADD INDEX (execute_time);
	END IF;
END; //
CALL alterTask() //
DROP PROCEDURE IF EXISTS alterTask //
DELIMITER ;


CREATE TABLE IF NOT EXISTS inmate
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	email CHAR(60) NOT NULL DEFAULT "",
	inmate_number CHAR(30) NOT NULL DEFAULT "",
	phone_number CHAR(20) NOT NULL DEFAULT "",
	credit BIGINT UNSIGNED NOT NULL DEFAULT 0, /*in 0.0001 USD*/
	create_time INT UNSIGNED NOT NULL DEFAULT 0,
	trial_expiry INT UNSIGNED NOT NULL DEFAULT 0,
	billing_day TINYINT UNSIGNED NOT NULL DEFAULT 0,
	service_expiry INT UNSIGNED NOT NULL DEFAULT 0,
	corrlinks_password CHAR(20) NOT NULL DEFAULT "",
	corrlinks_first_name CHAR(60) NOT NULL DEFAULT "",
	corrlinks_last_name CHAR(60) NOT NULL DEFAULT "",
	corrlinks_session TEXT NOT NULL,

	PRIMARY KEY (id),
	UNIQUE KEY (email),
	INDEX (phone_number)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS phone_number
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	inmate_number CHAR(30) NOT NULL DEFAULT "",
	phone_number CHAR(20) NOT NULL DEFAULT "",

	PRIMARY KEY (id),
	INDEX (inmate_number),
	UNIQUE KEY (phone_number)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS email_account
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	email CHAR(60) NOT NULL DEFAULT "",
	password CHAR(32) NOT NULL DEFAULT "",
	server CHAR(32) NOT NULL DEFAULT "",

	PRIMARY KEY (id),
	UNIQUE KEY (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS processed_email
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	email CHAR(60) NOT NULL DEFAULT "",
	folder CHAR(20) NOT NULL DEFAULT "",
	uid CHAR(16) NOT NULL DEFAULT "",
	time INT UNSIGNED NOT NULL DEFAULT 0,

	PRIMARY KEY (id),
	INDEX (email),
	INDEX (folder),
	INDEX (uid),
	INDEX (time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS processed_sms
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	uid CHAR(50) NOT NULL DEFAULT "",
	time INT UNSIGNED NOT NULL DEFAULT 0,

	PRIMARY KEY (id),
	INDEX (uid),
	INDEX (time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS refill_order
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	payment_id CHAR(16) NOT NULL DEFAULT "",
	inmate_number CHAR(30) NOT NULL DEFAULT "",
	email CHAR(60) NOT NULL DEFAULT "",
	amount INT UNSIGNED NOT NULL DEFAULT 0, /*0.01 USD*/
	invoice_id CHAR(20) NOT NULL DEFAULT "",
	status TINYINT UNSIGNED NOT NULL DEFAULT 0,
	time INT UNSIGNED NOT NULL DEFAULT 0,

	PRIMARY KEY (id),
	UNIQUE KEY (invoice_id),
	INDEX (payment_id),
	INDEX (inmate_number),
	INDEX (email),
	INDEX (status),
	INDEX (time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELIMITER //
DROP PROCEDURE IF EXISTS alterRefillOrder //
CREATE PROCEDURE alterRefillOrder()
BEGIN
	IF NOT EXISTS(
		SELECT NULL
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE table_name = 'refill_order'
		AND column_name = 'platform_transaction_id'
	)
	THEN
		ALTER TABLE `refill_order` ADD `platform_transaction_id` CHAR(255) NOT NULL default '';
		ALTER TABLE `refill_order` ADD INDEX (platform_transaction_id);
	END IF;

	IF NOT EXISTS(
		SELECT NULL
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE table_name = 'refill_order'
		AND column_name = 'phone'
	)
	THEN
		ALTER TABLE `refill_order` ADD `phone` CHAR(30) NOT NULL default '';
		ALTER TABLE `refill_order` ADD INDEX (phone);
	END IF;
END; //
CALL alterRefillOrder() //
DROP PROCEDURE IF EXISTS alterRefillOrder //
DELIMITER ;

CREATE TABLE IF NOT EXISTS support_message
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	inmate_number CHAR(30) NOT NULL DEFAULT "",
	processed TINYINT UNSIGNED NOT NULL DEFAULT 0,
	subject CHAR(255) NOT NULL DEFAULT "",
	content TEXT NOT NULL,
	time INT UNSIGNED NOT NULL DEFAULT 0,

	PRIMARY KEY (id),
	INDEX (processed),
	INDEX (time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS corrlinks_message
(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	inmate_number CHAR(30) NOT NULL DEFAULT "",
	subject CHAR(255) NOT NULL DEFAULT "",
	content TEXT NOT NULL,
	message_time INT UNSIGNED NOT NULL DEFAULT 0,
	hash CHAR(64) NOT NULL DEFAULT "",
	time INT UNSIGNED NOT NULL DEFAULT 0,

	PRIMARY KEY (id),
	INDEX (inmate_number),
	INDEX (subject),
	INDEX (hash),
	INDEX (time)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

COMMIT;

